//
//  UIAlertController.h
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 23/12/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIAlertController (Extended)

+ (void)alertaSimple:(NSString *)title message:(NSString *)message viewController:(UIViewController *)viewController;
+ (UIAlertController *)alertaErrorConMensaje:(NSString *)message;
+ (UIAlertController*)alertaModificable:(NSString *)titulo message:(NSString *)mensaje viewController:(UIViewController *)viewController;
+ (UIAlertController *)alertaAvisoConMensaje:(NSString *)message;
@end