//
//  confViewController.h
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 21/10/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//


#import "FXForm.h"
#import "AnalisisQuery.h"
#import "TipoAnalisisQuery.h"
#import<CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>
#import "MainViewController.h"




@interface SettingsViewController : FXFormViewController<CLLocationManagerDelegate>



@property (weak, nonatomic) IBOutlet UIView *alertaView;

@property (nonatomic, assign)   int error;
-(SettingsViewController*) init;
@end
