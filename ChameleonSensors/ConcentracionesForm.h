//
//  ConcentracionesForm.h
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 27/12/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FXForm.h"




@interface ConcentracionesForm : UIViewController<FXForm>

@property (nonatomic, assign) NSNumber* concentracion1;
@property (nonatomic, assign) NSNumber* concentracion2;
@property (nonatomic, assign) NSNumber* concentracion3;
@property (nonatomic, assign) NSNumber* concentracion4;
@property (nonatomic, assign) NSNumber* concentracion5;




@end
