//
//  BaseDatosAcceso.m
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 14/12/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//


#import "AnalisisQuery.h"



@implementation AnalisisQuery

/**
 * @brief Constructor AnalisisQuery
 * @param (NSString *)dbFilename nombre de la base de datos
 * @return instancetype
 */
-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename{
    self = [super init];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(
                                                         NSDocumentDirectory,
                                                         NSUserDomainMask,
                                                         YES);
    self.documentsDirectory = [paths objectAtIndex:0];
    

    [DBAccess setDelegate:self];
    [DBAccess openDatabaseNamed:dbFilename];
    
    return self;
}




/**
 * @brief Función que elimina todo análisis
 */
-(void)deleteTodos
{
    DBResultSet* rsTodos = [[Analisis query] fetch];
    
    [rsTodos removeAll];
    
}

/**
 * @brief Función que elimina un análisis por id
 * @param (NSNumber*)idAnalisis
 */
-(void)elminarConID: (NSNumber*)idAnalisis{
    NSLog(@"id:\n%@",idAnalisis);
    DBResultSet* rsAnalisisId = [[[Analisis query]
                                  whereWithFormat:@"Id=%@d", idAnalisis]
                                 fetch];
    [rsAnalisisId removeAll];
    
    
}


/**
 * @brief Función que añade un análisis
 * @param (double)colorR
 * @param (double)colorG
 * @param (double)colorB
 * @param (float)MFe
 * @param (float)PpmFe
 * @param (NSMutableArray*)DataMuestras
 * @param (NSMutableArray*)DataDesconocida
 * @param (NSMutableArray*)dataAjuste
 * @param (NSString*)UrlImagen
 * @param (NSString*)Observacion
 * @param (NSString*)Fecha
 * @param (float)longitud
 * @param (float)latitud
 * @param (NSString*)titulo
 * @param (NSNumber*)idTipoAnalisis
 */
-(void)addFila: (double)colorR conColorG:(double)colorG conColorB:(double)colorB conMFe:(float)MFe conPpmFe:(float)PpmFe conDataMuestras:(NSMutableArray*)DataMuestras conDataDesconocida:(NSMutableArray*)DataDesconocida conDataAjuste:(NSMutableArray*)dataAjuste conUrlImagen:(NSString*)UrlImagen conObservacion:(NSString*)Observacion conFecha:(NSString*)Fecha conLongitud:(float)longitud conLatitud:(float)latitud conTitulo:(NSString*)titulo conIdTipoAnalisis:(NSNumber*)idTipoAnalisis
{
    Analisis* analisis = [Analisis new];   
    analisis.longitud =longitud;
    analisis.latitud=latitud;
    analisis.colorR = colorR;
    analisis.colorG = colorG;
    analisis.colorB = colorB;
    analisis.MFe=MFe;
    analisis.PpmFe=PpmFe;
    analisis.DataMuestras=DataMuestras;
    analisis.DataDesconocida=DataDesconocida;
    analisis.UrlImagen=UrlImagen;
    analisis.Observacion=Observacion;
    analisis.Fecha=Fecha;
    analisis.idTipoAnalisis=idTipoAnalisis;
    analisis.titulo=titulo;
    analisis.DataAjuste=dataAjuste;
    
    // save the object into the table
    [analisis commit];
}

/**
 * @brief Función devuelve en un resultSet todos análisis
 * @return (DBResultSet*)análisis
 */
-(DBResultSet *)seleccionarTodos{
    
    DBResultSet* rsTodos = [[Analisis query] fetch];

    return rsTodos;
}

/**
 * @brief Función edita un análisis en función de su id
 * @param (int)idNum
 * @param (int)age 
 * @param (NSString*)fn
 * @param (NSString*)ln
 */
-(void)actualizarConID: (int)idNum withAge:(int)age withFN:(NSString*)fn withLN:(NSString*)ln{
    DBResultSet* r = [[[[Analisis query]
                        whereWithFormat:@"Id=%d",  idNum]
                       limit:1]
                      fetch];
    Analisis* jew = [r objectAtIndex:0];
    [jew setValue:fn forKey:@"colorR"];
    [jew setValue:ln forKey:@"lastname"];
    [jew setValue:[NSNumber numberWithInt:age] forKey:@"age"];
    [jew commit];
    
}

/**
 * @brief Función devuelve el análisis que coincide con el identificador
 * @param (NSNumber*)identificador
 * @return (DBResultSet*)
 */
-(DBResultSet *)getAnalisisArray:(NSNumber*)identificador
{
    return [[[Analisis query]
                                  whereWithFormat:@"idTipoAnalisis = %@", identificador]
                                 fetch];
}

/**
 * @brief Función devuelve true si existe el análisis con el id
 * @param (NSNumber*)identificador
 * @return (bool)
 */
-(BOOL)getAnalisis:(NSNumber*)identificador
{
    NSInteger tipoAnalisis = [[[[Analisis query]
                                whereWithFormat:@"idTipoAnalisis = %@", identificador]
                               fetch] count];
    
    if (tipoAnalisis > 0 ){
        return true;
    }
    
    return false;
    
}

/**
 * @brief Función devuelve el último análisis añadido
 * @return (NSArray*)
 */
-(NSArray*)getLastId
{
DBResultSet* r = [[[Analisis query]
                   orderBy:@"Id"]
                  fetch];
   return  [r lastObject];

}



/**
 * @brief Función establece nuevo titulo a un análisis
 * @param (Analisis*)obj
 * @param (NSString*)titulo
 */
-(void)setTituloID:(Analisis*)obj conTitulo:(NSString*)titulo
{
    
    [obj setTitulo:titulo];
    [obj commit];
    
}

/**
 * @brief Función establece nuevo observación a un análisis
 * @param (Analisis*)obj
 * @param (NSString*)observacion
 */
-(void)setObservacionID:(Analisis*)obj conObervacion:(NSString*)observacion
{
    
    [obj setObservacion:observacion];
    [obj commit];
    
}


/**
 * @brief Función establece nuevo observación a un análisis
 * @param (Analisis*)obj
 * @param (NSString*)observacion
 */
-(void)addObservacion:(Analisis*)objeto newObservacion:(NSString*)observacion{

    [objeto setValue:observacion forKey:@"Observacion"];

    [objeto commit];
}




@end