//
//  Settings.h
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 17/12/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FXForm.h"




@interface SettingsForm : NSObject<FXForm>

@property (nonatomic, assign) UISwitch *gpsSwitch;
@property (nonatomic, copy) NSString *tipoAnalisis;



@end
