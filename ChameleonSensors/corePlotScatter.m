//
//  corePlotScatter.m
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 6/11/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "corePlotScatter.h"

#import "editImageViewController.h"

#import "MBProgressHUD.h"

@implementation corePlotScatter

@synthesize graph,graphData,graphData2;

//valores minimos y máximos de los ejes de coordenadas.
///&lt; valor minimo del eje x
float xAxisMin;
///&lt; valor máximo del eje x
float xAxisMax;
///&lt; valor minimo del eje y
float yAxisMin;
///&lt; valor maximo del eje y
float yAxisMax;

CPTScatterPlot *plot, *plot2,*plotRectaAjuste;

CPTMutableLineStyle *lineStyle;



/**
 * @brief Constructor corePlotScatter
 */
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initialisePlot];
   

}


-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
    
    }
    [super viewWillDisappear:animated];
}


/**
 * @brief Función que configura la gráfica, tamaño ejes, colores.
 */
-(void)initialisePlot
{

    CGRect frame = [viewGraphCorePlot bounds];
    self.graph = [[CPTXYGraph alloc] initWithFrame:frame];
    
    self.graph.plotAreaFrame.paddingTop = 30.0f;
    self.graph.plotAreaFrame.paddingRight = 20.0f;
    self.graph.plotAreaFrame.paddingBottom = 50.0f;
    self.graph.plotAreaFrame.paddingLeft = 20.0f;
    
    
    [[self.graph defaultPlotSpace] setAllowsUserInteraction:YES];
    [self.graph defaultPlotSpace].delegate = self;
    
    
    
    viewGraphCorePlot.hostedGraph = self.graph;
    
    [self.graph applyTheme:[CPTTheme themeNamed:kCPTSlateTheme]];
    
    CPTMutableTextStyle *textStyle2 = [CPTMutableTextStyle textStyle];
    textStyle2.color = [CPTColor darkGrayColor];
    textStyle2.fontName = @"Helvetica-Bold";
    textStyle2.fontSize = 18.0f;
    NSString *title = @"Representación muestras";
    self.graph.title = title;
    self.graph.titleTextStyle = textStyle2;

    
    lineStyle = [CPTMutableLineStyle lineStyle];
    lineStyle.lineColor = [CPTColor whiteColor];
    lineStyle.lineWidth = 2.0f;
    
    CPTMutableTextStyle *textStyle = [CPTMutableTextStyle textStyle];
    textStyle.fontName = @"Helvetica";
    textStyle.fontSize = 14;
    textStyle.color = [CPTColor whiteColor];
    
    CPTPlotSymbol *plotSymbol = [CPTPlotSymbol crossPlotSymbol];
    plotSymbol.lineStyle = lineStyle;
    plotSymbol.size = CGSizeMake(8.0, 8.0);
    
    
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)self.graph.defaultPlotSpace;
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(xAxisMin) length:CPTDecimalFromFloat(xAxisMax - xAxisMin)];
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(yAxisMin) length:CPTDecimalFromFloat(yAxisMax - yAxisMin)];
    
    
   
    
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)self.graph.axisSet;
    
    axisSet.xAxis.title = NSLocalizedString(@"Componente principal",nil) ;
    axisSet.xAxis.titleTextStyle = textStyle;
    axisSet.xAxis.titleOffset = 30.0f;
    axisSet.xAxis.axisLineStyle = lineStyle;
    axisSet.xAxis.majorTickLineStyle = lineStyle;
    axisSet.xAxis.minorTickLineStyle = lineStyle;
    axisSet.xAxis.labelTextStyle = textStyle;
    axisSet.xAxis.labelOffset = 3.0f;
    axisSet.xAxis.majorIntervalLength = CPTDecimalFromFloat(1.0f);
    axisSet.xAxis.minorTicksPerInterval = 1;
    axisSet.xAxis.minorTickLength = 5.0f;
    axisSet.xAxis.majorTickLength = 7.0f;
    
    axisSet.yAxis.title = NSLocalizedString(@"Concentración (ppm)",nil);
    axisSet.yAxis.titleTextStyle = textStyle;
    axisSet.yAxis.titleOffset = 40.0f;
    axisSet.yAxis.axisLineStyle = lineStyle;
    axisSet.yAxis.majorTickLineStyle = lineStyle;
    axisSet.yAxis.minorTickLineStyle = lineStyle;
    axisSet.yAxis.labelTextStyle = textStyle;
    axisSet.yAxis.labelOffset = 3.0f;
    axisSet.yAxis.majorIntervalLength = CPTDecimalFromFloat(10.0f);
    axisSet.yAxis.minorTicksPerInterval = 1;
    axisSet.yAxis.minorTickLength = 5.0f;
    axisSet.yAxis.majorTickLength = 7.0f;
    

    plot = [[CPTScatterPlot alloc] init];
    plot.dataSource = self;
    plot.identifier = @"mainplot";
    plot.dataLineStyle = nil;
    plot.plotSymbol = plotSymbol;
    
    [self.graph addPlot:plot];
    
    //muestra desconocida
    plot2 = [[CPTScatterPlot alloc] init];
    plot2.dataSource = self;
    plot2.identifier = @"desconocido";
    plot2.dataLineStyle = nil;
    plot2.plotSymbol = plotSymbol;
    
    [self.graph addPlot:plot2];
    
    
    
    plotRectaAjuste = [[CPTScatterPlot alloc] init];
    plotRectaAjuste.dataSource = self;
    plotRectaAjuste.identifier = @"RectaAjuste";
    plotRectaAjuste.dataLineStyle = lineStyle;
    plotRectaAjuste.cornerRadius = 2.0;
    plotRectaAjuste.plotSymbol = nil;
    
    [self.graph addPlot:plotRectaAjuste];
    
    CPTMutableLineStyle *mainPlotLineStyle = [[plotRectaAjuste dataLineStyle] mutableCopy];
    [mainPlotLineStyle setLineWidth:1.0f];
    [mainPlotLineStyle setLineColor:[CPTColor colorWithCGColor:[[UIColor greenColor] CGColor]]];
    
    [plotRectaAjuste setDataLineStyle:mainPlotLineStyle];
    
    
    int numPuntos = 0;
    //Añadimos las anotaciones
    numPuntos =[graphData count];
    for (int i=0;i<numPuntos;i++) {
        NSValue *value = [self.graphData objectAtIndex:i];
        CGPoint point = [value CGPointValue];
    
        [self addAnotacion:point texto:[NSString stringWithFormat:@"%d:(%.01f , %.01f)",i+1,point.x,point.y]];
                   
    }
    
    numPuntos =[graphData2 count];
    for (int i=0;i<numPuntos;i++) {
        NSValue *value = [self.graphData2 objectAtIndex:i];
        CGPoint point = [value CGPointValue];
        
        [self addAnotacion:point texto:[NSString stringWithFormat:@"Desconocida\n(%.01f , %.01f)",point.x,point.y]];
        
    }

 
}

/**
 * @brief Función que establece el tipo de línea que aparece en la gráfica
 * @param (CPTMutableLineStyle*)tipo de línea
 */
-(void)setDataLine:(CPTMutableLineStyle*)tipo
{
    plot.dataLineStyle = tipo;
    
}

/**
 * @brief Función que añade anotaciones en la gráfica
 * @param (CGPoint)posicion puntos (x,y) donde se localizará la anotación 
 * @param (NSString*)texto de la anotación
 */
-(void)addAnotacion:(CGPoint)posicion texto:(NSString*)texto
{
    CPTPlotSpaceAnnotation *symbolTextAnnotation;
    
    if ( symbolTextAnnotation ) {
        [self.graph.plotAreaFrame.plotArea removeAnnotation:symbolTextAnnotation];
        symbolTextAnnotation = nil;
    }
    
    CPTMutableTextStyle *hitAnnotationTextStyle = [CPTMutableTextStyle textStyle];
    hitAnnotationTextStyle.color    = [CPTColor blackColor];
   
        hitAnnotationTextStyle.fontSize = 12.0;
    hitAnnotationTextStyle.fontName = @"Helvetica";
    
    NSNumber *x          = [NSNumber numberWithInt:posicion.x];
    NSNumber *y          = [NSNumber numberWithInt:posicion.y];
    NSArray *anchorPoint = [NSArray arrayWithObjects:x, y, nil];
    
    NSString *yString = [NSString stringWithFormat:texto];
    
    CPTTextLayer *textLayer = [[CPTTextLayer alloc] initWithText:yString style:hitAnnotationTextStyle];
    symbolTextAnnotation              = [[CPTPlotSpaceAnnotation alloc] initWithPlotSpace:graph.defaultPlotSpace anchorPlotPoint:anchorPoint];
    symbolTextAnnotation.contentLayer = textLayer;
    symbolTextAnnotation.displacement = CGPointMake(6.0, 12.0);
    [graph.plotAreaFrame.plotArea addAnnotation:symbolTextAnnotation];
}


+(void)setxAxisMin:(int)valor
{
    xAxisMin =valor;
}

+(void)setyAxisMin:(int)valor
{
    yAxisMin =valor;
}

+(void)setxAxisMax:(int)valor
{
    xAxisMax =valor;
}

+(void)setyAxisMax:(int)valor
{
    yAxisMax =valor;
}

+(float)getxAxisMin
{
    return xAxisMin;
}

+(float)getyAxisMin
{
    return yAxisMin;
}

+(float)getxAxisMax
{
    return xAxisMax;
}

+(float)getyAxisMax
{
    return yAxisMax;
}


/**
 * @brief Función devuelve el numero de puntos que tiene cada gráfica.
 * @param (CPTPlot *)plot gráfica
 * @return NSUInteger numero de puntos
 */
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    if ( [plot.identifier isEqual:@"mainplot"] )
    {
        return [self.graphData count];
        
    }else if([plot.identifier isEqual:@"RectaAjuste"]){
        
        return [self.graphRectaAjuste count];
        
    } else{
        return 1;
        
    }
    
    return 0;
}

/**
 * @brief Función que devuelve el valor del punto por indice
 * @param (CPTPlot *)plot gráfica
 * @param (NSUInteger)fieldEnum eje x o y
 * @param (NSUInteger)index indice
 * @return NSUInteger numero de puntos
 */
-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    NSValue *value;
    if ( [plot.identifier isEqual:@"mainplot"] )
    {
        value = [self.graphData objectAtIndex:index];
    }else if([plot.identifier isEqual:@"RectaAjuste"]){
    value = [self.graphRectaAjuste objectAtIndex:index];
    
    }else {
        value = [self.graphData2 objectAtIndex:index];
    }
        CGPoint point = [value CGPointValue];
        
        if ( fieldEnum == CPTScatterPlotFieldX )
        {
            return [NSNumber numberWithFloat:point.x];
        }
        else	
        {
            return [NSNumber numberWithFloat:point.y];
        }	
    
    
    return [NSNumber numberWithFloat:0];
}

/**
 * @brief Función que devuelve el símbolo de cada punto del gráfico
 * @param (CPTScatterPlot *)aPlot gráfico
 * @param (NSUInteger)index indice
 * @return CPTPlotSymbol* simbolo
 */
- (CPTPlotSymbol *)symbolForScatterPlot:(CPTScatterPlot *)aPlot recordIndex:(NSUInteger)index
{
    CPTPlotSymbol *plotSymbol;
    
    if ([[aPlot identifier] isEqual:@"mainplot"])
    {
        plotSymbol = [CPTPlotSymbol ellipsePlotSymbol];
        [plotSymbol setSize:CGSizeMake(10, 10)];
        [plotSymbol setFill:[CPTFill fillWithColor:[CPTColor colorWithComponentRed:71.0f/255.0f green:186.0f/255.0f blue:186.0f/255.0f alpha:1]]];
        
        
    }else if([[aPlot identifier]  isEqual:@"RectaAjuste"]){
        

        plotSymbol = nil;
        
    }else
    {
        plotSymbol = [CPTPlotSymbol ellipsePlotSymbol];
        plotSymbol.lineStyle = lineStyle;
        plotSymbol.size = CGSizeMake(8.0, 8.0);
        
        [plotSymbol setFill:[CPTFill fillWithColor:[CPTColor colorWithComponentRed:255.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1]]];
    }

    
    
    return plotSymbol;
}





@end
