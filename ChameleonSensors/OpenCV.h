//
//  openCV.h
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 30/10/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import <opencv2/opencv.hpp>
#import <opencv2/core/core.hpp>
#import <opencv2/highgui/highgui.hpp>
#import <opencv2/imgproc/imgproc_c.h>
#import <opencv2/core/core_c.h>
#import <opencv2/cvconfig.h>

#import <UIKit/UIKit.h>

@interface openCV : NSObject


+(cv::Mat) cvMatFromUIImage:(UIImage *)image;
+(UIImage *) UIImageFromCVMat:(cv::Mat)cvMat;

+(void) setErosionSize:(int)tam;
+(int) getErosionSize;
+(void) setDilatationSize:(int)tam;
+(int) getDilatationSize;
+(void) Erosion:(cv::Mat *)imagen forma:(cv::MorphShapes)forma;
+(void) Dilation:(cv::Mat *)imagen forma:(cv::MorphShapes)forma;
+(cv::Mat) converToGris:(cv::Mat)imagen;
+(cv::Mat) converTo:(int)tipo conImagen:(cv::Mat)imagen;
+(cv::Mat) filtroGaussiano:(cv::Mat)imagen conTamFiltro:(cv::Size)tamFiltro conSigmaX:(double)sigmaX conSigmaY:(double)sigmaY;
+(cv::Mat) algoritmoCanny:(cv::Mat)imagen conUmbralCanny:(double)umbralCanny conRatioCanny:(double)ratioCanny conGradiente:(BOOL)gradiente;
+(cv::Scalar) calcularMediaColores:(cv::Mat)imagen radio:(int)radio centro:(cv::Point2f)centro;
@end