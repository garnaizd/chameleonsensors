//
//  TipoAnalisisViewController.h
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 22/12/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIAlertController.h"

@protocol TipoAnalisisViewControllerDelegate

-(void)editingInfoWasFinished;

@end


@interface TipoAnalisisViewController :  UITableViewController <UITableViewDelegate, UITableViewDataSource,UINavigationControllerDelegate>

@property (nonatomic, strong) id<TipoAnalisisViewControllerDelegate> delegate;


@property (weak, nonatomic) IBOutlet UITableView *tblTipoAnalisis;
@property (nonatomic) int tipoAnalasisIDToEdit;

@property (nonatomic) int error;
-(void)editingInfoWasFinished;
@end
