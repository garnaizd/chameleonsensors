


//
//  TipoAnalisisCell.h
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 22/12/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TipoAnalisisCell : UITableViewCell<UITableViewDelegate>

///&lt;  icono tipo análisis
@property (nonatomic, strong) IBOutlet UIImageView *icono;
///&lt;  nombre del tipo de análisis
@property (nonatomic, strong) IBOutlet UILabel *nombre;
///&lt;  limite legal del tipo de análisis
@property (nonatomic, strong) IBOutlet UILabel *limiteLegal;
///&lt;  tipo de análisis
@property (weak, nonatomic) IBOutlet UILabel *txtTipoAnalisis;
///&lt;  limite legal
@property (weak, nonatomic) IBOutlet UILabel *txtLimiteLegal;

@end