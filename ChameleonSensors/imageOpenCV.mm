//
//  mathOpenCV.m
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 28/10/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "imageOpenCV.h"

@interface imageOpenCV ()
///&lt; color rojo
@property (nonatomic) double         colorR;
///&lt; color verde
@property (nonatomic) double        colorG;
///&lt; color azul
@property (nonatomic) double        colorB;
///&lt;  mol/l
@property (nonatomic) float             MFe;
///&lt; parte por millón
@property (nonatomic) float             PpmFe;
///&lt; Imagen a procesar
@property (nonatomic) UIImage*          imagenConCirculos;
///&lt; Array de las muestras conocidas
@property (nonatomic) NSMutableArray*   dataMuestras;
///&lt; Array de las muestras desconocidas
@property (nonatomic) NSMutableArray*   dataDesconocida;
///&lt; Array con los puntos de la recta de ajuste
@property (nonatomic) NSMutableArray*   rectaAjuste;
 ///&lt; numero de error generado
@property (nonatomic) int   error;
///&lt; puntos eje x
@property (nonatomic) cv::Mat   x;
///&lt; puntos eje y
@property (nonatomic) cv::Mat   y;




//metodos privados
/**
 * @brief Detectar circulos
 * @param UIImage* imagen imagen original
 * @return vector<Point2f>
 */
-(std::vector<cv::Point2f>)detectarCirculos:(UIImage*)imagen;
/**
 * @brief Función que hace el cálculo mantematico (PCA)
 * @param vector<Point2f>)centros detectados en la imagen
 */
-(void)procesamientoMatematico:(std::vector<cv::Point2f>)centros;
/**
 * @brief Genera puntos a partir de los coeficientes de ajuste para representar la recta o parábola
 * @param (cv::Mat)coeficientes
 * @param (int)orden del ajuste
 * @param (cv::Mat)MuestrasTotalX muestras pca
 * @return NSMutableArray* numero de puntos generados
 */
-(NSMutableArray*)generarPuntos:(cv::Mat)coeficientes conOrden:(int)orden conMuestrasTotalX:(cv::Mat)MuestrasTotalX;

@end



@implementation imageOpenCV




using namespace cv;
using namespace std;

//Variables globales
string errorAnalisis;
Mat matImagenGris,matImagenGaus,matImagen;


NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
// CONCENTRACIONES PATRON EN ppm




//componente principal desconocida
///&lt; componente principal desconocida
float xTEST;

//concentracion desconcida
///&lt; concentración desconocida
float yTEST;

//numero muestras conocidas
///&lt; numero de muestras conocidas
const int nMuestras = 5;




//colores de todas las muestras
cv::Scalar arrayMediasRGBCirculos[6];

//imagen que se muestra por pantalla

Mat imagenOriginal;

//key-value de centros y radios
map<int,int> centroRadioMap;


/**
 * @brief Constructor imageOpenCV
 * @param UIImage* imagen
 * @return id
 */
- (id)initAnalisis:(UIImage*)imagen {
    self = [super init];
    
    @try{
        vector<Point2f> circulos = [self detectarCirculos:imagen];
    
        if (circulos.size() == 6){
    
            [self procesamientoMatematico:circulos];
         
           
    
            self.colorR =  arrayMediasRGBCirculos[0][0];
            self.colorG =  arrayMediasRGBCirculos[0][1];
            self.colorB =  arrayMediasRGBCirculos[0][2];
           // self.PpmFe  = yTEST;
            self.MFe    = ((yTEST/1000)/56);
  
        }else{
            self.error = 1;
        }
        
    }@catch (NSException *exception) {
        self.error = 1;
    }
    
    return self;
}




-(int)getError
{
    return _error;
}
-(NSMutableArray*)getDataDesconocida
{
    return _dataDesconocida;
}

-(NSMutableArray*)getDataMuestras
{
    return _dataMuestras;
}

-(void)setDataDesconocida:(NSMutableArray*)data
{
    _dataDesconocida=data;
}

-(NSMutableArray*)getRectaAjuste
{
    return _rectaAjuste;
}

-(void)setRectaAjuste:(NSMutableArray*)data
{
    _rectaAjuste=data;
}

-(void)setDataMuestras:(NSMutableArray*)data
{
    _dataMuestras=data;
}

-(UIImage*)getImagenConCirculos
{

    return _imagenConCirculos;
}

-(void)setImagenConCirculos:(UIImage*)imagen
{
    _imagenConCirculos =  imagen;
}

-(double)getColorR
{
    return _colorR;
}

-(double)getColorG
{
    return _colorG;
}

-(double)getColorB
{
    return _colorB;
}

-(void)setColorR:(double)colorR
{
     _colorR =colorR ;
}

-(void)setColorG:(double)colorG
{
    _colorG =colorG ;
}

-(void)setColorB:(double)colorB
{
    _colorB =colorB ;
}

-(void)setMFe:(float)MFe
{
    _MFe=MFe;
}

-(void)setPpmFe:(float)PpmFe
{
     _PpmFe=PpmFe;
}

-(float)getMFe
{
    return _MFe;
}

-(float)getPpmFe
{
    return _PpmFe;
}


/**
 * @brief Detectar circulos
 * @param UIImage* imagen imagen original
 * @return vector<Point2f>
 */
- (vector<Point2f>)detectarCirculos:(UIImage*)imagen
{
    
    //Estructura que ordena los centros
    
    struct ordenarAscPoint2 {
        bool operator() (int pt1, int pt2) { return (pt1 > pt2);}
    } puntos2;
    
    struct ordenarCentros {
        bool operator() (Point2f pt1, Point2f pt2) { return (pt1.x < pt2.x);}
    } puntos3;
    
    
    
    //Convertimos UIImage a tipo Mat
    matImagen = [openCV cvMatFromUIImage:imagen];
    
    
    
    //Hacemos una copia de la imagen original, muy importante
    imagenOriginal = matImagen.clone();
    
    imagenOriginal = [openCV converTo:CV_8UC3 conImagen:imagenOriginal];
    
    
    cv::Size tamaño = matImagen.size();
    
    cout << "Numero de columnas (eje x): " << tamaño.width  << endl;
    cout << "Numero de filas (eje y)   : " << tamaño.height << endl;
    
    
    //convertimos la imagen a gris
    
   matImagenGris = [openCV converToGris:imagenOriginal];
    
    
    //hacemos un filtro 2d gaussiano
    
   matImagenGaus =  [openCV filtroGaussiano:matImagenGris conTamFiltro:cv::Size(7,7) conSigmaX:1 conSigmaY:0];
    

    
    Mat threshold_output;
    vector<vector<cv::Point> > contours;
    vector<Vec4i> hierarchy;
    int thresh = 150;
    RNG rng(12345);
    
    threshold( matImagenGaus, threshold_output, thresh, 255, THRESH_BINARY );
    
    [openCV setErosionSize:10];
    [openCV Erosion:&threshold_output forma:MORPH_ELLIPSE];
    
    [openCV setDilatationSize:20];
    [openCV Dilation:&threshold_output forma:MORPH_RECT];

    
    cv::findContours(threshold_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0) );
    
    
   
    vector<int> pos(contours.size() );
    vector<double> perimeter(contours.size() );
    
    
    

    
    for(int i =0;i<pos.size();i++){
        pos[i]=99;
    }
    
    for( int i = 0; i < contours.size(); i++ ){
        perimeter[i] = cv::arcLength(contours[i],true);
    }
    
    
    vector<vector<cv::Point> > contours_poly( contours.size() );
    vector<cv::Rect> boundRect( contours.size() );
    vector<Point2f>center2( contours.size() );
    vector<float>radius( contours.size() );
    
    
    
    for( int i = 0; i < contours.size(); i++ ){
        approxPolyDP( Mat(contours[i]), contours_poly[i], 3, true );
        boundRect[i] = boundingRect( Mat(contours_poly[i]) );
        minEnclosingCircle( (Mat)contours_poly[i], center2[i], radius[i] );
    }


    
    double perimA;
    double perimB = 0.0;
    float xA = 0.0, xB=0.0;
    
 
    double perimetroMedio = 0.0 , xAMedio = 0.0;
    int contador = 0;
    for(int i =0;i<perimeter.size();i++){
        perimA = perimeter[i];
        xA = center2[i].y;
        for(int j =i+1;j<perimeter.size();j++){
            perimB = perimeter[j];
            xB = center2[j].y;
            if(cv::abs(perimA-perimB)<perimA*0.20 && cv::abs(xA-xB)<60 && contours[i].size()>40){
                contador = contador +1;
                if (contador == nMuestras){
                    break;
                }
            }
        }
        if(contador == nMuestras){
            
            perimetroMedio = perimA;
            xAMedio = xA;
            contador=0;
            break;
        }
        contador=0;
    }
    
     for(int i =0;i<perimeter.size();i++){
         if(cv::abs(perimeter[i] -perimetroMedio )>perimetroMedio* 0.20 || contours[i].size()<40
            || cv::abs(center2[i].y -xAMedio )>60){
             pos[i]=i;
         }
     }
    
 
    std::sort(pos.begin(), pos.end(), puntos2);
    
    
    for(int i =0 ;i<pos.size();i++){
        if(pos[i]!=99){
            contours.erase(contours.begin() + pos[i]);
        }
    }
    

     vector<Moments> mu(contours.size() );
    for( int i = 0; i < contours.size(); i++ ){
        //obtenemos contornos
        mu[i] = moments( contours[i], false );
        //obtenemos centros
        

        
    }
    
    
    NSLog(@"Numero de circulos detectados %lu",contours.size());
    
    vector<vector<cv::Point> > contours_poly2( contours.size() );
    vector<cv::Rect> boundRect2( contours.size() );
    vector<Point2f>center3( contours.size() );
    vector<float>radius2( contours.size() );

    
    
    for( int i = 0; i < contours.size(); i++ ){
        approxPolyDP( Mat(contours[i]), contours_poly2[i], 3, true );
        boundRect2[i] = boundingRect( Mat(contours_poly2[i]) );
        minEnclosingCircle( (Mat)contours_poly2[i], center3[i], radius2[i] );
    }
    
    Mat drawing = matImagen.clone();
    vector<Point2f> centros( contours.size() );
    centroRadioMap.clear();
    for( int i = 0; i< contours.size(); i++ )
    {
        Scalar color = Scalar( 255, 0, 0);
        cv::drawContours( drawing, contours_poly2, i, color, 1, 8, vector<Vec4i>(), 0, cv::Point() );
        circle( drawing, center3[i], (int)radius2[i], color, 2, 8, 0 );
        //rectangle( drawing, boundRect[i].tl(), boundRect[i].br(), color, 2, 8, 0 );
        centros[i] = center3[i];
        centroRadioMap[center3[i].x] = radius2[i];

    }
    _imagenConCirculos = [openCV UIImageFromCVMat:drawing];
    std::sort(centros.begin(), centros.end(), puntos3);

    
    return centros;
    
}



/**
 * @brief Función que hace el cálculo mantematico (PCA)
 * @param vector<Point2f>)centros detectados en la imagen
 */
-(void)procesamientoMatematico:(vector<Point2f>)centros
{
    Mat   yTmp;
    float concentraciones[] = {[[defaults objectForKey:@"Concentracion1"]floatValue], [[defaults objectForKey:@"Concentracion2"]floatValue], [[defaults objectForKey:@"Concentracion3"]floatValue], [[defaults objectForKey:@"Concentracion4"]floatValue], [[defaults objectForKey:@"Concentracion5"]floatValue]};
    
    Mat BGR(nMuestras+1,3,CV_32FC1,Scalar::all(0));
    
    Scalar media;
    Scalar stdar;
    
    vector<Vec3f> muestras;
    
    
    //acceder a los centros (menos el desconocido)
    for (int n = 0; n < centros.size(); n++)
    {
        cv::Point2f centro = centros[n];
        NSLog(@"centorx %f",centro.x);
        NSLog(@"centory %f",centro.y);
        
        //calculamos la media de pixeles de la sub imagen
        arrayMediasRGBCirculos[n] = [openCV calcularMediaColores:matImagen radio:centroRadioMap[centro.x] centro:centro];
        
        //todas las muestras, conocidas y desconocidas
        muestras.push_back(Vec3f(arrayMediasRGBCirculos[n][0],arrayMediasRGBCirculos[n][1],arrayMediasRGBCirculos[n][2]));
        
    }
    
    meanStdDev(muestras, media, stdar);
    
    //cogemos todas las muestras
    for(int i=0; i<nMuestras+1; i++)
    {
        for(int c=0;c<3;c++)
        {
            //RGB
            BGR.at<float>(i,c) = (muestras[i][c] - media(c))/stdar(c);
        }
    }
    cout << "BGR  : " << BGR << endl;
    
    // ANALISIS POR COMPONENTES PRINCIPALES: PCA
    PCA pca_analysis(BGR, Mat(), CV_PCA_DATA_AS_ROW);
    
    // PCA members
    Mat eigen_vals = pca_analysis.eigenvalues.clone();
    Mat eigen_vecs = pca_analysis.eigenvectors.clone();
    Mat mean_vecs  = pca_analysis.mean.clone();
    
    
    cout << "valores propios:"  << endl << eigen_vals << endl;
    cout << "vectores propios:" << endl << eigen_vecs << endl;
    
    // Cambio de base
    Mat MuestrasTotalX =pca_analysis.project(BGR);
    
    Mat xtmp(nMuestras,1, CV_32FC1,Scalar::all(0));
    
    _x=xtmp.clone();
    
    //comprobar este relleno, de fila 1 ala ultima de la primera columna
    for(int i=0;i<nMuestras;i++){
        _x.at<float>(i,0)=MuestrasTotalX.at<float>(i+1,0);
        
    }
    
    
    _y = Mat(_x.size(),_x.type(),concentraciones);
    yTmp = _y .clone();
    

    NSLog(@"%f",_y.at<float>(0));
    
    // AJUSTE POR MINIMOS CUADRADOS DE ORDEN n
    int orden = 0;
    // Coeficientes
    // coef = [a0 a1 ... an]' -> y(x) = ao + a1.x + ... + an.x^n
    Mat coef_iter, coefFit;
    // Valor estimado
    Mat y_iter, yFit;
    // Desviacion estandar del ajuste
    float std_iter, stdFit = 1e8;
    // Sistema de n+1 ecuaciones
    // Construimos matriz de Vandermonde
    //      | 1 x x^2 ... x^n |
    //  M = | 1 x x^2 ... x^n |
    //      | . . .      . . .|
    //      | 1 x x^2 ... x^n |
    //
    const int MaxOrden = 2;
    Mat M = Mat::ones(_x.rows, MaxOrden+1,CV_32FC1);
    Mat xBuffer = _x.clone();
    xBuffer.copyTo(M.col(1));
    for(int i=2;i<=MaxOrden;i++)
    {
        xBuffer = xBuffer.mul(xBuffer);
        xBuffer.copyTo(M.col(i));
    }
    // Calculamos ajustes de orden n=1 y orden n=2
    for(int n=1;n<=MaxOrden;n++)
    {
        Mat Mn = M(Range::all(),Range(0,n+1)).clone();
        Mat LeastSq = Mn.t()*Mn;
        Mat yBuffer = Mn.t()*yTmp;
        
        // Vector columna de coeficientes
        coef_iter = LeastSq.inv()*yBuffer;
        
        // Estimacion
        y_iter = Mn*coef_iter;
        
        // ERROR: Desviacion estandar
        Mat errorSq = (yTmp - y_iter).mul((yTmp - y_iter));
        float Sr_lin = sum(errorSq)(0);
        std_iter = sqrt(Sr_lin/(nMuestras - (n+1)));
        
        
        // Seleccion del "mejor ajuste"
        if(std_iter < stdFit)
        {
            orden = n;
            stdFit = std_iter;
            coefFit = coef_iter.clone();
            yFit = y_iter.clone();
        }
    }
    
    
    cout << "RESULTADO:" << endl;
    cout << "Orden ajuste          : " << orden << endl;
    cout << "Desviacion ajuste     : " << stdFit << endl;
    cout << "Coeficientes ajuste   : " << coefFit << endl;
    cout << "Concentracion Estimada: " << yFit << endl;
    
    
    // ...
    // CONCENTRACION PROBLEMA
    // ...
    
    xTEST = MuestrasTotalX.at<float>(0,0);
    yTEST = coefFit.at<float>(0,0);
    for(int i=1;i<=orden;i++)
    {
        yTEST = yTEST + xTEST*coefFit.at<float>(i,0);
        xTEST = xTEST*xTEST;
    }
    xTEST = MuestrasTotalX.at<float>(0,0);
    
    cout << "xTEST: " << xTEST << endl;
    cout << "yTEST: " << yTEST << endl;
    

    
    
    
    NSMutableArray*   dataDesconocida= [[NSMutableArray alloc] init];
    NSMutableArray*   dataConocida = [[NSMutableArray alloc] init];
       int tam = _x.rows;
    
    
    
    float cpMayor = 0;
    float cpMenor = 10000000;
    
    for(int i=0; i< tam;i++){
        //componente principal
        float a = _x.at<float>(i,0);
        
        if (a < cpMenor)
            cpMenor = a;
        
        if (a  > cpMayor)
            cpMayor = a;
        //concentracciones
        float b = _y.at<float>(i,0);
        
        [dataConocida addObject:[NSValue valueWithCGPoint:CGPointMake(a, b)]];
        
        
    }
    
    [dataDesconocida addObject:[NSValue valueWithCGPoint:CGPointMake(xTEST, yTEST)]];
    [self setPpmFe:yTEST];
    [self setDataDesconocida:dataDesconocida];
    [self setDataMuestras:dataConocida];
    
    
    [self generarPuntos:coefFit conOrden:orden conMuestrasTotalX:MuestrasTotalX];
}


/**
 * @brief Genera puntos a partir de los coeficientes de ajuste para representar la recta o parábola
 * @param (cv::Mat)coeficientes
 * @param (int)orden del ajuste
 * @param (cv::Mat)MuestrasTotalX muestras pca
 * @return NSMutableArray* numero de puntos generados
 */
-(NSMutableArray*)generarPuntos:(cv::Mat)coeficientes conOrden:(int)orden conMuestrasTotalX:(cv::Mat)MuestrasTotalX
{
    NSMutableArray*   rectaAjuste = [[NSMutableArray alloc] init];

    
    int numPuntos=10;

    
    yTEST = coeficientes.at<float>(0,0);
    int contador = 0;
    int pts2=-10;
    int x=0;
    bool bandera = false;
    for(int pts=-10;pts<numPuntos;pts++){
        for(int i=1;i<=orden;i++){
             yTEST = yTEST + pts*coeficientes.at<float>(i,0);
             pts = pts*pts;
            
        }
            contador = contador +1;
        if(contador>1 || bandera ==true){
            x=pts2+contador;
        }else{
            x=pts2;
            contador =contador -1;
            bandera=true;
        }
            [rectaAjuste addObject:[NSValue valueWithCGPoint:CGPointMake(x, yTEST)]];
            pts= x;
    

         yTEST = coeficientes.at<float>(0,0);
        
    }
    
    
 
    NSLog(@"total%lu",(unsigned long)rectaAjuste.count);
    [self setRectaAjuste:rectaAjuste];
    
    return nil;
}



@end