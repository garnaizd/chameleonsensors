//
//  TipoAnalisis.h
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 22/12/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <DBAccess/DBAccess.h>

@interface TipoAnalisis : DBObject

///&lt;  nombre del tipo de análisis
@property (strong) NSString*         nombre;
///&lt;  limite legal del tipo de análisis
@property double         limiteLegal;

@end