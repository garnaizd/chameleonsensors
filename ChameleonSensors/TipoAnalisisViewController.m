//
//  TipoAnalisisViewController.m
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 22/12/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import "TipoAnalisisViewController.h"
#import "TipoAnalisisQuery.h"
#import "TipoAnalisisCell.h"
#import "AnalisisQuery.h"
#import "SettingsViewController.h"


@interface TipoAnalisisViewController (){
    SettingsViewController *settingsForm ;
}


@property (nonatomic, strong) NSArray *arrTipoAnalisis;


@property (nonatomic, strong) TipoAnalisisQuery *dbManager;
@property (nonatomic, strong) AnalisisQuery *dbManagerAnalisis;


@property (nonatomic) TipoAnalisis* objetoTipoAnalisis;
@property (nonatomic) Analisis* objetoAnalisis;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnAdd;

- (void)mostrarGuardarToolbar;
-(void)loadData;
@end





@implementation TipoAnalisisViewController


/**
 * @brief Constructor de TipoAnalisisViewController
 */
-(TipoAnalisisViewController*) init {
    self = [super init];

    return self;
}


/**
 * @brief Función que se ejecuta con el inicio de la vista
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    _objetoTipoAnalisis = nil;
    [self mostrarGuardarToolbar];
    
    
    
    
    self.dbManager = [[TipoAnalisisQuery alloc]
                      initWithDatabaseFilename:@"ChameleonSensorsDB"];
    
    self.dbManagerAnalisis = [[AnalisisQuery alloc]
                             initWithDatabaseFilename:@"ChameleonSensorsDB"];
    
    self.tblTipoAnalisis.delegate = self;
    self.tblTipoAnalisis.dataSource = self;
    
    
    self.title = NSLocalizedString(@"Tipos de análisis", nil);
    
    // Load the data.
    [self loadData];
    
 
}
/**
 * @brief Función que se ejecuta con el cierre de la vista
 */
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    if(self.error == 1 ) {
        self.error = 0;
        [self crearAlertaAddTipoAnalisis];
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)mostrarGuardarToolbar {
    [self.navigationItem setRightBarButtonItems:@[self.btnAdd] animated:YES];
}


/**
 * @brief Función que inicia la alerta para añadir tipos de análisis
 * @param (id)sender
 * @param IBAction acción
 */
- (IBAction)AddTipoAnalisis:(id)sender {
    
    [self crearAlertaAddTipoAnalisis];
    
   
    
}

/**
 * @brief Función que guarda la información introducida por el usuario del tipo de análisis
 */
-(void)crearAlertaAddTipoAnalisis
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Tipo Análisis", nil)
                                                                             message:NSLocalizedString(@"Añada un tipo de análisis", nil)
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* Guardar = [UIAlertAction actionWithTitle:NSLocalizedString(@"Guardar",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                              {
                                  UIAlertController *alertaError;
                                  
                                  if(![[alertController.textFields firstObject].text  isEqual: @""] && ![[alertController.textFields lastObject].text  isEqual: @""]){
                                      
                                      if( ![self.dbManager addFila:[alertController.textFields firstObject].text conLimiteLegal:[alertController.textFields lastObject].text] ){
                                          alertaError = [UIAlertController alertaErrorConMensaje:[NSString stringWithFormat:@"%@ %@", [alertController.textFields firstObject].text , NSLocalizedString(@"ya existe", nil)]];
                                          [self presentViewController:alertaError animated:YES completion:nil];
                                      }else{
                                          [self editingInfoWasFinished];
                                          
                                          [self.delegate editingInfoWasFinished];
                                          
                                          settingsForm = [[SettingsViewController alloc] init];
                                          settingsForm.error = 1;
                                          [self performSegueWithIdentifier:@"tipoAnalisisSettingsSegue" sender:self];
                                          
                                          
                                      }
                                      
                                  }else if([[alertController.textFields firstObject].text  isEqual: @""] || [[alertController.textFields lastObject].text  isEqual: @""]){
                                      alertaError = [UIAlertController alertaErrorConMensaje:NSLocalizedString(@"Rellene todos los campos", nil)];
                                      [self presentViewController:alertaError animated:YES completion:nil];
                                  }
                                  
                                  
                              }];
    
    UIAlertAction* cancelar = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancelar",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                               {
                                   
                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textFieldNombre) {
        textFieldNombre.placeholder = NSLocalizedString(@"Nombre",nil);
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textFieldLimite) {
        textFieldLimite.placeholder =NSLocalizedString(@"Limite legal",nil) ;
        textFieldLimite.keyboardType = UIKeyboardTypeNumberPad;
    }];
    
    [alertController addAction:cancelar];
    [alertController addAction:Guardar];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    
}

/**
 * @brief Función que realiza la transición entre vistas
 * @param (UIStoryboardSegue *)segue vista final
 * @param (id)sender
 */
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
    if([segue.identifier isEqualToString:@"tipoAnalisisSettingsSegue"]){
        if(settingsForm.error==1){
            settingsForm = (SettingsViewController *)segue.destinationViewController;
            settingsForm.error=1;
        }
    }
}


/**
 * @brief Función que activa la edición de la celda, tipo de análisis
 * @param (UITableView *)tableView
 * @param (NSIndexPath *)indexPath indice de la celda a editar
 * @return bool, true si se puede, false si no se puede editar
 */
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}


/**
 * @brief Función que edita el análisis seleccionado por el usuario.esta funcío comprueba si está siendo usado en caso de que el usuario quiera eliminarlo.
 * @param (UITableView *)tableView
 * @param (UITableViewCellEditingStyle)editingStyle forma de edición
 * @param (NSIndexPath *)indexPath indice de la celda a editar
 */
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            
            
            TipoAnalisis* objetoFila = [self.arrTipoAnalisis objectAtIndex:indexPath.row];
            
            
            //comprobamos que no haya ningun analisis con este tipoAnalisis
            if ([self.dbManagerAnalisis getAnalisis:objetoFila.Id] == false)
            {
                [objetoFila remove];
                [self loadData];
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

                if(objetoFila.nombre == [defaults objectForKey:@"tipoAnalisis"] ){
                
                    defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setObject:@"None" forKey:@"tipoAnalisis"];
                    [defaults synchronize];
                    
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:@"loadData" object:nil];
            }else{
                UIAlertController *alertaError = [UIAlertController alertaErrorConMensaje:NSLocalizedString(@"Este tipo análisis está siendo utilizado en historial", nil)];
                [self presentViewController:alertaError animated:YES completion:nil];
            }
        }
        

}


/**
 * @brief Función que devuelde el numero de celdas por sección.
 * @param (UITableView *)tableView
 * @param (NSInteger)section sección
 * @return NSInteger numero de celdas
 */
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSLog(@"numero elementos: %lu", (unsigned long)self.arrTipoAnalisis.count);
    
    return self.arrTipoAnalisis.count;
    
}


/**
 * @brief Función que devuelde el numero secciones que hay en la tabla
 * @param (UITableView *)tableView
 * @return NSInteger numero de secciones
 */
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

/**
 * @brief Función que ejecuta una alerta cuando el ususario selecciona una celda de tipo de análisis. Esta alerta tiene 2 textfield: tipo de análisis y limite legal.
 * @param (UITableView *)tableView
 * @param (NSIndexPath *)indexPath  indice de la celda seleccionada
 */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TipoAnalisis* objetoFila = [self.arrTipoAnalisis objectAtIndex:indexPath.row];
    
    _objetoTipoAnalisis = objetoFila;
    
    NSLog(@"%@",_objetoTipoAnalisis );
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Tipo Análisis", nil)
                                                                             message:NSLocalizedString(@"Datos guardados", nil)
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* Guardar = [UIAlertAction actionWithTitle:NSLocalizedString(@"Guardar",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                              {
                                  [self.dbManager actualizarFilaConID:objetoFila conNombre:[alertController.textFields firstObject].text conLimiteLegal:[[alertController.textFields lastObject].text doubleValue]];
                                  [self editingInfoWasFinished];
                                  
                              }];
    
    UIAlertAction* cancelar = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancelar",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                               {
                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.text = objetoFila.nombre;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.text =[NSString stringWithFormat:@"%.02f", objetoFila.limiteLegal] ;
    }];
    
    
    [alertController addAction:cancelar];
    [alertController addAction:Guardar];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}


/**
 * @brief Función que devuelve los valores asociados a cada celda. En este caso son tipo de análisis y limite legal. Para consultar estos datos se hacen consultas al ORM.
 * @param (UITableView *)tableView
 * @param (NSIndexPath *)indexPath  indice de la celda seleccionada
 * @return UITableViewCell celda con la información tipo de análisis y limite legal
 */
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    TipoAnalisisCell *cell = [tableView dequeueReusableCellWithIdentifier:@"idCellTipoAnalisis" forIndexPath:indexPath];
    
    TipoAnalisis* analisisCelda = [self.arrTipoAnalisis objectAtIndex:indexPath.row];
    

    cell.txtTipoAnalisis.text =NSLocalizedString(@"Tipo analisis:", nil);
    
    cell.txtLimiteLegal.text =NSLocalizedString(@"Limite legal:", nil);
    
    
    cell.nombre.text = analisisCelda.nombre;
    cell.limiteLegal.text = [NSString stringWithFormat:@"%.02f", analisisCelda.limiteLegal] ;
    

    
    return cell;
}



/**
 * @brief Función que devuelve la altura por celda
 * @param (UITableView *)tableView
 * @param (NSIndexPath *)indexPath  indice de la celda
 * @return CGFloat altura por celda
 */
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 84.0;
}




/**
 * @brief Función que recarga la tabla
 */
-(void)loadData{
    

    NSArray* r = self.dbManager.seleccionarTodos;
    
    if (self.arrTipoAnalisis != nil) {
        self.arrTipoAnalisis = nil;
    }
    
    if(r.count == 0){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:@"None" forKey:@"tipoAnalisis"];
        [defaults synchronize];
        
    }

    self.arrTipoAnalisis = [[NSArray alloc] initWithArray: r];
    

    
    
    NSLog(@"%@",self.arrTipoAnalisis);
    
    
    
    // Recargamos la vista
    [self.tblTipoAnalisis reloadData];
    
    
}


/**
 * @brief Función que llama arecargar la tabla
 */
-(void)editingInfoWasFinished{
    // Recargamos la informacion de la tabla
    [self loadData];
}


@end