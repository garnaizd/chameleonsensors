//
//  ViewController.m
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 5/10/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import "MainViewController.h"
#import "AnalisisQuery.h"
#import "TipoAnalisisQuery.h"
#import "AnalisisCell.h"
#import "UIAlertController.h"
#import "RTWalkthroughPageViewController.h"
#import "RTWalkthroughViewController.h"

#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad


@interface MainViewController ()<PhotoTweaksViewControllerDelegate>
{
    TipoAnalisisViewController *tipoAnalisisViewController;
    SettingsViewController *settingsViewController;
}



///&lt; imagen seleccionada por el usario
@property (retain, nonatomic) IBOutlet UIImageView *imagenSelecionada;
///&lt; texto indicando si no hay análisis
@property (weak, nonatomic) IBOutlet UILabel *textInformativoInicio;

///&lt; imagen con la cámara, en caso de que no haya análisis
@property (weak, nonatomic) IBOutlet UIButton *imgInformativoInicio;
///&lt; Segmented control, para realizar el filtrado por tipos de análisis
@property (weak, nonatomic) IBOutlet UISegmentedControl *SegmenTiposAnalisis;

///&lt; objeto AnalisisQuery
@property (nonatomic, strong) AnalisisQuery *dbManager;
///&lt; objeto TipoAnalisisQuery
@property (nonatomic, strong) TipoAnalisisQuery *dbManagerTipoAnalisis;

///&lt; Vista del pickerView
@property (weak, nonatomic) IBOutlet UIView *viewFiltrado;

///&lt; objeto Analisis
@property (nonatomic) Analisis* objetoAnalisis;
///&lt; objeto TipoAnalisis
@property (nonatomic) TipoAnalisis* objetoTipoAnalisis;

-(NSInteger)comprobarNumeroTipoAnalisis;
- (void) crearDirectorio;
-(void)actualizarObjectoAnalisis;
-(void)loadData;
- (NSDateFormatter*)obtenerFechaString;

@end





@implementation MainViewController


@synthesize fechaCreacion;



/**
 * @brief Función que muestra action sheet en caso de que no haya análisis generados.
 * @param (UIButton *)sender botones cámara y galería
 * @return IBAction acción generada al pulsar un botón
 */
- (IBAction)imgInformativoInicio:(UIButton *)sender {

    UIAlertController *alertController;
    UIAlertAction *camara;
    UIAlertAction *galeria;
    
    alertController = [UIAlertController alertControllerWithTitle:nil
                                                          message:nil
                                                   preferredStyle:UIAlertControllerStyleActionSheet];
    camara = [UIAlertAction actionWithTitle:NSLocalizedString(@"Camara", nil)
                                             style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction *action) {
                                               [self tomarFotoFunc];
                                           }];
    galeria = [UIAlertAction actionWithTitle:NSLocalizedString(@"Galeria", nil)
                                           style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction *action) {
                                             [self seleccionarImagenFunc];
                                         }];
    [alertController addAction:camara];
    [alertController addAction:galeria];
    [alertController setModalPresentationStyle:UIModalPresentationPopover];
    
    UIPopoverPresentationController *popPresenter = [alertController
                                                     popoverPresentationController];
    popPresenter.sourceView = _imgInformativoInicio;
    popPresenter.sourceRect = _imgInformativoInicio.bounds;
    [self presentViewController:alertController animated:YES completion:nil];
    
}

/**
 * @brief Función que notifica si ha cambiado el usuario la selecciónd el segmented control
 * @param (id)sender
 * @return IBAction acción generada al pulsar un botón
 */
- (IBAction)SegmentValueChanged:(id)sender {
    NSArray* r;
    switch ([sender selectedSegmentIndex]) {
        case 0:
           r = self.dbManager.seleccionarTodos;

            self.arrAnalisis = [[NSArray alloc] initWithArray: r];
            
            [self.tblAnalisis reloadData];
            
            [_ViewPicker reloadAllComponents];
            [self.tblAnalisis reloadData];
            [_ViewPicker setHidden:YES];
            break;
        case 1:
            _arrTiposAnalisis = self.dbManagerTipoAnalisis.seleccionarTodos;
            [_ViewPicker reloadAllComponents];
            [self.tblAnalisis reloadData];
            [_ViewPicker setHidden:NO];
            break;

    }
    
    
}

/**
 * @brief Función que consulta a el valor del tipo de análisis seleccionado
 */
-(void)actualizarObjectoAnalisis
{
    
    @try {
        
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* tipoanalisis =[defaults objectForKey:@"tipoAnalisis"];
    
    
    _objetoTipoAnalisis = [[self.dbManagerTipoAnalisis getObjetoConNombre:tipoanalisis] objectAtIndex:0];
        
    }@catch (NSException *exception) {
        
    }
    
}



/**
 * @brief Función que se ejecuta antes del incio de la vista e inicializa la interfaz
 */
- (void)viewWillAppear:(BOOL)animated {
    [self.tblAnalisis reloadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                                  selector:@selector(loadObserver:)
                                                       name:@"loadData" object:nil];
    
    [_SegmenTiposAnalisis setTitle:NSLocalizedString(@"Todos", nil) forSegmentAtIndex:0];
    [_SegmenTiposAnalisis setTitle:NSLocalizedString(@"Filtrar análisis", nil) forSegmentAtIndex:1];
}

/**
 * @brief Función que se ejecuta al cerrar la vista. Elimina los observadores activos al evento loadData
 */
-(void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:@"loadData"];
}

/**
 * @brief Función que se ejecuta en cada inicio. inicializa la vista
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL presented = [defaults boolForKey:@"walkthroughPresented"];
    
    if (!presented) {
        [self showWalkthrough:nil];
        
        [defaults setBool:YES forKey:@"walkthroughPresented"];
        [defaults synchronize];
    }
    

    self.title = NSLocalizedString(@"Analisis", nil);
    self.dbManager = [[AnalisisQuery alloc]
                      initWithDatabaseFilename:@"ChameleonSensorsDB"];
    
    self.dbManagerTipoAnalisis = [[TipoAnalisisQuery alloc]
                                  initWithDatabaseFilename:@"ChameleonSensorsDB"];
    
    _objetoAnalisis = nil;
    
    
    
    [self actualizarObjectoAnalisis];
   
    
    self.tblAnalisis.delegate = self;
    self.tblAnalisis.dataSource = self;
    
    self.ViewPicker.delegate=self;
    self.ViewPicker.dataSource = self;

    

    [self crearDirectorio];
    // Do any additional setup after loading the view, typically from a nib.

    
    
    
    // Load the data.
    [self loadData];
    


}

/**
 * @brief Función que muestra el storyboard de ayuda
 * @param (id)sender
 * @return IBAction acción mostrar storyboard
 */
- (IBAction)showWalkthrough:(id)sender {
    
    UIStoryboard *stb;
    if ( IDIOM == IPAD ) {
        stb = [UIStoryboard storyboardWithName:@"Walkthrough" bundle:nil];
    }else{
       stb = [UIStoryboard storyboardWithName:@"WalkthroughiPhone" bundle:nil];

    }
    RTWalkthroughViewController *walkthrough = [stb instantiateViewControllerWithIdentifier:@"walk"];
    
    RTWalkthroughPageViewController *pageOne = [stb instantiateViewControllerWithIdentifier:@"walk1"];
    RTWalkthroughPageViewController *pageTwo = [stb instantiateViewControllerWithIdentifier:@"walk2"];
    RTWalkthroughPageViewController *pageThree = [stb instantiateViewControllerWithIdentifier:@"walk3"];
    
    walkthrough.delegate = self;
    [walkthrough addViewController:pageOne];
    [walkthrough addViewController:pageTwo];
    [walkthrough addViewController:pageThree];
    
    [self presentViewController:walkthrough animated:YES completion:nil];
}

/**
 * @brief Delegate que controla el cierre de la ayuda
 * @param (RTWalkthroughViewController *)controller  controlador
 */
- (void)walkthroughControllerDidClose:(RTWalkthroughViewController *)controller {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


/**
 * @brief Función que devuelve el número de componentes del pickerView
 * @param (UIPickerView *)pickerView
 * @return NSInteger numero de componentes
 */
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
     return 1;
}

/**
 * @brief Función que devuelve el número de filas que tiene el pickerView
 * @param (UIPickerView *)pickerView
 * @return NSInteger numero de filas
 */
- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    
    return _arrTiposAnalisis.count;
    
}


/**
 * @brief Función que incializa los titulos de cada elemento del pickerView, para ello consulta a la base de datos los tipos de análisis almacenados
 * @param (UIPickerView *)pickerView
 * @param (NSInteger)row fila
 * @param (NSInteger)component componente
 * @return NSInteger numero de filas
 */
- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    @try{
        Analisis *analisis =  _arrTiposAnalisis[row];
        return [self.dbManagerTipoAnalisis getTipoAnalisis:[analisis Id]];
        
    }@catch (NSException *exception) {
            return @"None";
        }
}


/**
 * @brief Función que detecta la fila seleccionada por el ususario. Una vez seleccionada una fila, actualiza el array arrAnalisis
 * @param (UIPickerView *)pickerView
 * @param (NSInteger)row fila
 * @param (NSInteger)component componente
 */
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{

    self.arrAnalisis = [[NSArray alloc] initWithArray: [self.dbManager getAnalisisArray:[_arrTiposAnalisis[row] Id]]];
    
    
    [self.tblAnalisis reloadData];
    
}




/**
 * @brief Función que obtiene la posición actual del usario
 */
- (void)getCurrentLocation
{
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    
    while([CLLocationManager authorizationStatus] ==kCLAuthorizationStatusNotDetermined){
        [locationManager requestAlwaysAuthorization];
    }
    
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    
    [locationManager startUpdatingLocation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}







/**
 * @brief Función que crea el directorio ChameleonSensors, en caso de que no exista
 */
- (void) crearDirectorio
{
    
    Class PHPhotoLibrary_class = NSClassFromString(@"PHPhotoLibrary");
    
    if (PHPhotoLibrary_class)
    {
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^
         {
            //se comprueba si existe o no el directorio
             PHFetchOptions *fetchOptions = [PHFetchOptions new];
             fetchOptions.predicate = [NSPredicate predicateWithFormat:@"title == %@", @"ChameleonSensors"];
             PHFetchResult *fetchResult = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:fetchOptions];
             
             if (fetchResult.count == 0)
             {
                 //Creamos el album
                [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:@"ChameleonSensors"];
             }
             
         }
         
          completionHandler:^(BOOL success, NSError *error)
         {
             if (!success) {
                 NSLog(@"Error creating album: %@", error);
             }else{
                 NSLog(@"Perfecto");
             }
         }];
    }
    
    __block bool bandera = false;
    __block PHCollection *collectionChameleon;
    
    
    
    PHFetchResult *userAlbums = [PHCollectionList fetchTopLevelUserCollectionsWithOptions:nil];
    
    [userAlbums enumerateObjectsUsingBlock:^(PHAssetCollection *collection, NSUInteger idx, BOOL *stop) {
        NSLog(@"album title %@", collection.localizedTitle);
        
        if ([collection.localizedTitle isEqual: @"ChameleonSensors"])
            collectionChameleon = collection;
        bandera = true;
        
    }];
    
    
    //contiene todas las imagenes del album chameleon sensors
    self.assetCollection = (PHAssetCollection *)collectionChameleon;
    
}


/**
 * @brief Función que obtiene la fecha del sistema del dispositivo
 * @return NSDateFormatter* fecha
 */
- (NSDateFormatter*)obtenerFechaString {
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    
    return dateFormatter;
    
}


/**
 * @brief Función que recoge la imagen capturada con la cámara o seleccionada de la galería.
 * @param (UIImagePickerController*)picker controlador picker
 * @param (NSDictionary*)info información recogida por el delegate
 */
- (void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    [self crearDirectorio];
    _objetoAnalisis = nil;
    
    NSDateFormatter *fecha = [self obtenerFechaString];
    NSString *fechaString = [fecha stringFromDate:[NSDate date]];
    self.fechaCreacion = fechaString;
    
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {

        self.imagenSelecionada.image = info[UIImagePickerControllerOriginalImage];
        
    }else{
        
        self.imagenSelecionada.image = info[UIImagePickerControllerOriginalImage];
        
    }
    
    
    
     [self actualizarObjectoAnalisis];
    
    PhotoTweaksViewController *photoTweaksViewController = [[PhotoTweaksViewController alloc] initWithImage: self.imagenSelecionada.image];
    photoTweaksViewController.delegate = self;
    photoTweaksViewController.autoSaveToLibray = NO;
    [picker pushViewController:photoTweaksViewController animated:YES];
    
    
   
    
    
    
}

/**
 * @brief Función que recorta la imagen según la selección del usuario.
 * @param (UIImage *)image imagen sin recortar
 */
- (void)cropImage:(UIImage *)image{
    PhotoTweaksViewController *photoTweaksViewController = [[PhotoTweaksViewController alloc] initWithImage:image];
    photoTweaksViewController.autoSaveToLibray = NO;
    photoTweaksViewController.delegate = self;
    
    [[self navigationController] pushViewController:photoTweaksViewController animated:YES];
}

/**
 * @brief Delegate que detecta si el usuario ha finalizado el recorte de la imagen
 * @param (PhotoTweaksViewController *)controller
 * @param (UIImage *)croppedImage imagen recortada
 */
- (void)photoTweaksController:(PhotoTweaksViewController *)controller didFinishWithCroppedImage:(UIImage *)croppedImage
{
    
    self.imagenSelecionada.image=croppedImage;
    
    [self performSegueWithIdentifier:@"SegueImagen" sender:nil];
  
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
    
}

/**
 * @brief Delegate que cierra el crop en caso de que el usuario lo cancele
 * @param (PhotoTweaksViewController *)controller
 */
- (void)photoTweaksControllerDidCancel:(PhotoTweaksViewController *)controller
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:0] animated:YES];
    
    
}








//pasamos datos a editImageViewController
/**
 * @brief Función que gestiona las transaciones entre vistas del controlador actual
 * @param (UIStoryboardSegue *)segue vista final
 */
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
    if([segue.identifier isEqualToString:@"SegueImagen"]){
        editImageViewController *controller = (editImageViewController *)segue.destinationViewController;
        controller.imagen = self.imagenSelecionada.image;
        controller.fechaCreacion = self.fechaCreacion;
        controller.assetCollection = _assetCollection;
        controller.recordIDToEdit = self.recordIDToEdit;
        controller.objetoAnalisis = _objetoAnalisis;
        controller.objetoTipoAnalisis = _objetoTipoAnalisis;
        controller.delegate = self;
        
    }else if([segue.identifier isEqualToString:@"MainViewtipoAnalisisSegue"]){
        if(tipoAnalisisViewController.error==1){
            tipoAnalisisViewController = (TipoAnalisisViewController *)segue.destinationViewController;
            tipoAnalisisViewController.error=1;
        }
    }else if([segue.identifier isEqualToString:@"settingsMainViewSegue"]){
        if(settingsViewController.error==1){
            settingsViewController = (SettingsViewController *)segue.destinationViewController;
            settingsViewController.error=1;
        }
    }
    
}




//notifica si ha habido error al sacar la foto
/**
 * @brief Delegate que detecta si ha habido algún error durante la apertura de la cámara
 * @param (NSError *)error detectado
 */
- (void)image: (UIImage *) image didFinishSavingWithError: (NSError *)error contextInfo:(void *)contextInfo
{
    if (error != nil) {
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:@"Error"
                                    message:NSLocalizedString(@"Ha surgido un error al sacar la foto", nil)
                                    preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK accion")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Ha pulsado OK ");
                                   }];
        
        [alert addAction:okAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}



/**
 * @brief Función que devuelve el numero de tipos de análisis guardados en base de datos
 * @return (NSInteger) número de tipos de análisis disponibles
 */
-(NSInteger)comprobarNumeroTipoAnalisis
{
    
    NSArray* r = self.dbManagerTipoAnalisis.seleccionarTodos;
    
    
    return [[NSArray alloc] initWithArray: r].count;
}

/**
 * @brief Función que llama a la función que captura imágenes
 * @return (IBAction) acción
 */
//Sacar imagen
- (IBAction)tomarFoto:(id)sender
{
    [self tomarFotoFunc];
}

/**
 * @brief Función que comprueba si en Configuración hay algún tipo de análisis seleccionado por el usuario.
 * @return (bool) true si hay seleccionado un tipo de análisis, false sino lo hay
 */
-(bool)checkTipoAnalisisAvailable
{
    
       NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
       
    if([[defaults objectForKey:@"tipoAnalisis"] isEqual:@"None"]){
        return false;
    }
    return true;
}




/**
 * @brief Función que comprueba si hay algun tipo de análisis selecionado en Configuración.
 */
-(void)alertaAvailableTipoAnaliis{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:NSLocalizedString(@"Aviso", nil)
                                message:NSLocalizedString(@"Para generar un análisis debe seleccionar un tipo de análisis en configuración", nil)
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK accion")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   settingsViewController  = [[SettingsViewController alloc] init];
                                   settingsViewController.error = 1;
                                   [self performSegueWithIdentifier:@"settingsMainViewSegue" sender:self];
                               }];
    
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}




/**
 * @brief Función que notifica al usuario si no hay tipos de análisis añadidos en base de datos, sino los hay notifica al usuario.
 */
-(void)alertaNoAvailableTipoAnalisis{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:NSLocalizedString(@"Aviso", nil)
                                message:NSLocalizedString(@"Debe añadir un tipo de análisis para realizar un análisis", nil)
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK accion")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   tipoAnalisisViewController  = [[TipoAnalisisViewController alloc] init];
                                   tipoAnalisisViewController.error = 1;
                                   [self performSegueWithIdentifier:@"MainViewtipoAnalisisSegue" sender:self];
                               }];
    
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}



/**
 * @brief Función que accede a cámara o galería, y selecciona una imagen.También desactiva el flash de la cámara en caso de que lo haya.
 */
-(void)tomarFotoFunc
{
    
    if([self comprobarNumeroTipoAnalisis]==0){
        
        
        [self alertaNoAvailableTipoAnalisis];
        
        
        
        
        
    }else if(!self.checkTipoAnalisisAvailable && [self comprobarNumeroTipoAnalisis]!=0){
       
        [self alertaAvailableTipoAnaliis];
        
        
    }else{
    
        
        [self getCurrentLocation];
        
        self.recordIDToEdit = -1;
        
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            
            
            UIAlertController *alert = [UIAlertController
                                        alertControllerWithTitle:NSLocalizedString(@"Perdone", nil)
                                        message:NSLocalizedString(@"No hay cámara disponible", nil)
                                        preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"OK", @"OK accion")
                                       style:UIAlertActionStyleDefault
                                       handler:nil];
            
            [alert addAction:okAction];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            return;
            
            
            
            
        }
        
        
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        
        
        
        
        imagePickerController.allowsEditing = NO;
        imagePickerController.delegate = self;
        
        
        
        
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        if ([UIImagePickerController isFlashAvailableForCameraDevice:imagePickerController.cameraDevice] )
        {
            imagePickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeOff;
            
        }
        
        
        //mostramos en pantalla la imagen
        [self presentViewController:imagePickerController animated:YES completion:nil];
        
    }
}




//Cuando se seleciona una imagen
/**
 * @brief Función que llama a la función seleccionarImagenFunc
 */
- (IBAction)seleccionarImagen:(id)sender
{
    
    [self seleccionarImagenFunc];
  
}


/**
 * @brief Función que accede a imagePickerController
 */
-(void)seleccionarImagenFunc
{
    if([self comprobarNumeroTipoAnalisis]==0){
        
        [self alertaNoAvailableTipoAnalisis];
        
        
    }else if(!self.checkTipoAnalisisAvailable && [self comprobarNumeroTipoAnalisis]!=0){
        
        [self alertaAvailableTipoAnaliis];
        
    }else{
        
        self.recordIDToEdit = -1;
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        
        
        
        imagePickerController.delegate = self;
        
        imagePickerController.allowsEditing = false;
        
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        
        
        [self.navigationController presentViewController:imagePickerController animated:YES completion:nil];
        
    }
}


/**
 * @brief Delegaye que permite la edición de los análisis.
 */
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}


/**
 * @brief Función que elimina el análisis de la base de datos que el usario ha seleccionado
 * @param (UITableView *)tableView
 * @param (UITableViewCellEditingStyle)editingStyle
 * @param (NSIndexPath *)indexPath
 */
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        
        Analisis* objetoFila = [self.arrAnalisis objectAtIndex:indexPath.row];
        
        [objetoFila remove];
        

        [self loadData];
    }
}


/**
 * @brief Función que devuelve el numero de celdas por sección
 * @param (UITableView *)tableView
 * @param (NSInteger)section
 * @return (NSInteger)numero de celdas
 */
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSLog(@"numero elementos: %lu", (unsigned long)self.arrAnalisis.count);

    return self.arrAnalisis.count;

}


/**
 * @brief Función que devuelve el numero de secciones
 * @return (NSInteger)numero de secciones
 */
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


/**
 * @brief Delegate que inicia la transición a la vista de análisis para visualizar el análisis seleccionado el accesorio por el usuario.
 * @param (UITableView *)tableView
 * @param (NSIndexPath *)indexPath
 */
-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    
    
    Analisis* peep = [self.arrAnalisis objectAtIndex:indexPath.row];
    self.recordIDToEdit = [peep.Id intValue];
    
    // Perform the segue.
    [self performSegueWithIdentifier:@"SegueImagen" sender:self];
    
}

/**
 * @brief Delegate que inicia la transición a la vista de análisis para visualizar el análisis seleccionado por el usuario.
 * @param (UITableView *)tableView
 * @param (NSIndexPath *)indexPath
 */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    _objetoTipoAnalisis=nil;
    _objetoAnalisis =nil;
    
    Analisis* objetoFila = [self.arrAnalisis objectAtIndex:indexPath.row];
    
    _objetoAnalisis = objetoFila;
    
    _objetoTipoAnalisis = [[self.dbManagerTipoAnalisis getObjetoConId:objetoFila.idTipoAnalisis] objectAtIndex:0];
    

    
    [self performSegueWithIdentifier:@"SegueImagen" sender:self];
    
}



/**
 * @brief Delegate que inicializa las celdas, para ello consulta en base de datos
 * @param (UITableView *)tableView
 * @param (NSIndexPath *)indexPath
 * @return (UITableViewCell*) celda
 */
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    AnalasisCell *cell = [tableView dequeueReusableCellWithIdentifier:@"idCellAnalisis" forIndexPath:indexPath];
    
    Analisis* analisisCelda = [self.arrAnalisis objectAtIndex:indexPath.row];
    
    cell.TituloCell.text = analisisCelda.titulo;
    cell.FechaCell.text = analisisCelda.Fecha;
    cell.ConcentracionCell.text = [NSString stringWithFormat:@"%1.3f", analisisCelda.PpmFe];
    
    //cell.ImagenCell.image = [UIImage imageNamed:@"polimeroBase"];
    cell.ImagenCell.image = [UIImage imageWithContentsOfFile:analisisCelda.UrlImagen];
    
    
    cell.tipoAnalisis.text = [self.dbManagerTipoAnalisis getTipoAnalisis:analisisCelda.idTipoAnalisis];
    
    if([self esLegal:analisisCelda.PpmFe conTipoAnalisis:analisisCelda.idTipoAnalisis]){
        cell.ImagenLegalCell.image =[UIImage imageNamed:@"checkicon"];
        cell.txtlegalIlegal.text =NSLocalizedString(@"Legal", nil);
    }else{
        cell.ImagenLegalCell.image =[UIImage imageNamed:@"uncheckicon"];
        cell.txtlegalIlegal.text = NSLocalizedString(@"Ilegal", nil);
    }
    

    
    return cell;
}

/**
 * @brief Función que comprueba si es legal o no dependiendo del límite legal
 * @param (float)ppmFe
 * @param (NSNumber*)tipo
 * @return bool
 */
-(bool)esLegal:(float)ppmFe conTipoAnalisis:(NSNumber*)tipo
{
    
    if(ppmFe >[_dbManagerTipoAnalisis getLimiteLegal:tipo] )
    {
        return false;
    }
    return true;
    
}

/**
 * @brief Función que devuelve la altura de la celda
 * @param (UITableView *)tableView
 * @param (NSIndexPath *)indexPath
 * @return CGFloat altura
 */
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 105.0;
}




/**
 * @brief Función que recarga la tabla, actualizando los arrays con los datos de análisis.
 */
-(void)loadData{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString * tipo =[defaults objectForKey:@"tipoAnalisis"];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Tipo análisis",nil),tipo] style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.rightBarButtonItem.tintColor= [UIColor blackColor];
    
    NSArray* r = self.dbManager.seleccionarTodos;
    
    _arrTiposAnalisis = self.dbManagerTipoAnalisis.seleccionarTodos;
    
    if (self.arrAnalisis != nil) {
        self.arrAnalisis = nil;
    }
    self.arrAnalisis = [[NSArray alloc] initWithArray: r];
    
    // Recargamos la vista
    [self.tblAnalisis reloadData];
    [_ViewPicker reloadAllComponents];
    
    if (self.arrAnalisis == nil || [self.arrAnalisis count] == 0){
        _textInformativoInicio.text = NSLocalizedString(@"No Dispone De Ningún Análisis, presiones la cámara para empezar", nil);
        _textInformativoInicio.textAlignment = NSTextAlignmentCenter;

        [_textInformativoInicio setHidden:NO];
        [_viewFiltrado setHidden:YES];
        [_ViewPicker setHidden:YES];
        _imgInformativoInicio.hidden = NO;
        _imgInformativoInicio.enabled = YES;
        [self.view addSubview:_textInformativoInicio];
        [self.view addSubview: _imgInformativoInicio];
        NSLog(@"%d",_imgInformativoInicio.enabled);

    }else{
        
        [_textInformativoInicio setHidden:YES];
        [self.view addSubview:_textInformativoInicio];
        [_viewFiltrado setHidden:NO];
        _imgInformativoInicio.enabled = NO;
        _imgInformativoInicio.hidden = YES;
    }
}

/**
 * @brief Función que se ejecta por el obsrvador al detectar el inicio del evento.
 * @param (NSNotification *)note
 */
- (void)loadObserver:(NSNotification *)note {
   [ self editingInfoWasFinished];
}



/**
 * @brief Función que llama a recargar la vista
 */
-(void)editingInfoWasFinished{
    // Recargamos la informacion de la tabla
    [self loadData];
}




@end
