//
//  corePlotScatter.h
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 6/11/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "CorePlot.h"


@interface corePlotScatter : UIViewController <CPTScatterPlotDataSource,UIActionSheetDelegate,CPTPlotSpaceDelegate>
{
    CPTXYGraph *_graph;
    NSMutableArray * graphData;
    NSMutableArray * graphData2;
    
    IBOutlet CPTGraphHostingView *viewGraphCorePlot;
}



@property (nonatomic, retain) CPTXYGraph *graph;
@property (nonatomic, retain) NSMutableArray *graphData;
@property (nonatomic, retain) NSMutableArray *graphData2;
@property (nonatomic, retain) NSMutableArray *graphRectaAjuste;



-(void)initialisePlot;


+(void)setxAxisMin:(int)valor;

+(void)setyAxisMin:(int)valor;


+(void)setxAxisMax:(int)valor;

+(void)setyAxisMax:(int)valor;


+(float)getxAxisMin;


+(float)getyAxisMin;


+(float)getxAxisMax;


+(float)getyAxisMax;

-(void)setDataLine:(CPTMutableLineStyle*)tipo;

-(void)addAnotacion:(CGPoint)posicion texto:(NSString*)texto;


@end