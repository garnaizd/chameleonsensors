//
//  analisis.h
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 10/12/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TipoAnalisis.h"

@interface Analisis : DBObject
///&lt;  color rojo del análisis
@property double         colorR;
///&lt;  color verde del análisis
@property double        colorG;
///&lt;  color azul del análisis
@property double        colorB;
///&lt;  mol/l
@property float             MFe;
///&lt;  partes por millón de la concentración de hierro
@property float             PpmFe;
///&lt;  puntos de las muestras conocidas
@property NSMutableArray*               DataMuestras;
///&lt;  puntos de la muestra desconocida
@property NSMutableArray*               DataDesconocida;
///&lt;  puntos de la recta o parábola de ajuste
@property NSMutableArray*               DataAjuste;
///&lt;  url de la imagen asociada al análisis
@property (strong) NSString*         UrlImagen;
///&lt;  observación del análisis
@property (strong) NSString*         Observacion;
///&lt;  fecha del análisis
@property (strong) NSString*         Fecha;
///&lt;  titulo asociado al análisis
@property (strong) NSString*         titulo;
///&lt;  latitud gps
@property float             latitud;
///&lt;  longitud gps
@property float             longitud;
///&lt;  identificador asociado del tipoanalisis
@property (strong) NSNumber*         idTipoAnalisis;

@end