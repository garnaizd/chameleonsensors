//
//  UIAlertController.m
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 23/12/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import "UIAlertController.h"


@implementation UIAlertController (Extended)

/**
 * @brief Función muestra alerta
 * @param (NSString *)titulo
 * @param (NSString *)mensaje
 * @param (UIViewController *)viewController
 */
+ (void)alertaSimple:(NSString *)titulo
              message:(NSString *)mensaje
       viewController:(UIViewController *)viewController {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:titulo
                                          message:mensaje
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:nil]];
    
    [viewController presentViewController:alertController animated:YES completion:nil];
}


/**
 * @brief Función muestra error
 * @param (NSString *)mensaje
 */
+ (UIAlertController *)alertaErrorConMensaje:(NSString *)message
{
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:@"Error" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    [alert addAction:action];
    return alert;
}

/**
 * @brief Función muestra aviso
 * @param (NSString *)mensaje
 */
+ (UIAlertController *)alertaAvisoConMensaje:(NSString *)message
{
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Aviso", nil) message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    [alert addAction:action];
    return alert;
}

/**
 * @brief Función muestra alerta
 * @param (NSString *)titulo
 * @param (NSString *)mensaje
 * @param (UIViewController *)viewController
 * @return (UIAlertController*)
 */
+ (UIAlertController*)alertaModificable:(NSString *)titulo
             message:(NSString *)mensaje
      viewController:(UIViewController *)viewController {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:titulo
                                          message:mensaje
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:nil]];
    
    return alertController;
}


@end