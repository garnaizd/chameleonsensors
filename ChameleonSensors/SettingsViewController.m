//
//  confViewController.m
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 21/10/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//


#import "SettingsViewController.h"
#import "SettingsForm.h"
#import "UIAlertController.h"
#import "MainViewController.h"



@interface SettingsViewController ()


@property (nonatomic, strong) AnalisisQuery *dbManager;
@property (nonatomic, strong) TipoAnalisisQuery *dbManagerTipoAnalisis;

-(void)activarGps;
-(void)selectTipoAnalisis;
-(void)CerrarActualNaviationController;
-(void)submitBorrarHistorial:(UITableViewCell<FXFormFieldCell> *)cell;
@end

@implementation SettingsViewController

SettingsForm *form;
NSUserDefaults *defaults;

-(SettingsViewController*) init {
    self = [super init];
    
    return self;
}


/**
 * @brief Función que se ejecuta en cada inicio de vista
 */
- (void)viewDidLoad
{
    [super viewDidLoad];
        

    
    UIBarButtonItem *botonAtras = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Atrás", nil) style:UIBarButtonItemStylePlain target:self action:@selector(CerrarActualNaviationController)];
    
    
    self.navigationItem.leftBarButtonItem = botonAtras;
    
    
}

/**
 * @brief Función que se ejecuta en cada cierre de vista
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    if(self.error == 1){
        self.error = 0;
        UIAlertController *alertaError = [UIAlertController alertaAvisoConMensaje:NSLocalizedString(@"Seleccione el tipo de análisis en la sección Ajuste para poder realizar análisis", nil)];
        
        [self presentViewController:alertaError animated:YES completion:nil];
        

    }
    
    
}

/**
 * @brief Función que activa el gps
 */
-(void)activarGps
{
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    form = self.formController.form;
    
    
    if([form.gpsSwitch  isEqual: @1]){
          [locationManager requestWhenInUseAuthorization];
        

       [locationManager startUpdatingLocation];
        
        if ([CLLocationManager authorizationStatus] ==kCLAuthorizationStatusDenied){
            NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:appSettings];
            [locationManager stopUpdatingLocation];
        }
        
        
        while([CLLocationManager authorizationStatus] ==kCLAuthorizationStatusNotDetermined){
            [locationManager requestWhenInUseAuthorization];
        }
        
    }else{
        NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:appSettings];
    }
    
    
    
    
    defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:form.gpsSwitch forKey:@"gpsSettings"];
    
    
    [defaults synchronize];
}


/**
 * @brief Función que selecciona el tipo de análisis
 */
-(void)selectTipoAnalisis
{
    form = self.formController.form;
    defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:form.tipoAnalisis forKey:@"tipoAnalisis"];
    [defaults synchronize];
    
    
}

/**
 * @brief Función que se ejecuta cuando el usuario cierra la vista actual
 */
-(void)CerrarActualNaviationController
{

//hay que añadir un patron observador que refresque mainviewcontroller
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loadData" object:nil];
    
    
     [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:0] animated:YES];
}


/**
 * @brief Función inicializa el formulario
 */
- (void)awakeFromNib
{
    
    //set up form
    self.formController.form = [[SettingsForm alloc] init];


}


/**
 * @brief Función que borra todo el historial de análisis
 * @param (UITableViewCell<FXFormFieldCell> *)cell
 */
- (void)submitBorrarHistorial:(UITableViewCell<FXFormFieldCell> *)cell
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Aviso", nil)
                                                                             message:NSLocalizedString(@"Está seguro que desea borrar todo el historial?", nil)
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* aceptar = [UIAlertAction actionWithTitle:NSLocalizedString(@"Aceptar",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                              {
                                  //se hace la conexion a la BD
                                  self.dbManager = [[AnalisisQuery alloc]
                                                    initWithDatabaseFilename:@"ChameleonSensorsDB"];

                                  
                                  if([[NSArray alloc] initWithArray: [self.dbManager seleccionarTodos]].count ==0){
                                      
                                      UIAlertController *alertaError = [UIAlertController alertaAvisoConMensaje:NSLocalizedString(@"El historial está vacio", nil)];
                                      [self presentViewController:alertaError animated:YES completion:nil];
                                      
                                  }else{
                                  
                                      
                                      [self.dbManager deleteTodos];
                                      
                                      [[NSNotificationCenter defaultCenter] postNotificationName:@"loadData" object:nil];

                                    
                                      [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
                                  }
                               
                              }];
    
    UIAlertAction* cancelar = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancelar",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                               {
                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [alertController addAction:cancelar];
    [alertController addAction:aceptar];
    
    
    [self presentViewController:alertController animated:YES completion:nil];
}




@end
