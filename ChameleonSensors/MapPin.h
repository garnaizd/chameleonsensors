//
//  MapPin.h
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 30/12/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MapPin : NSObject <MKAnnotation>
@property(nonatomic,assign) CLLocationCoordinate2D coordinate;
@property(nonatomic,copy) NSString *titulo;
@property(nonatomic,copy) NSString *subtitulo;
@end

#import "MapPin.h"
@implementation MapPin
@end