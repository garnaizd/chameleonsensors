//
//  TipoAnalisisQuery.h
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 22/12/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TipoAnalisis.h"

@interface TipoAnalisisQuery : NSObject<DBDelegate>

@property (nonatomic, strong) NSString *databaseFilename;


-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename;

-(void)elminarConID: (NSNumber*)idAnalisis;
-(DBResultSet *)seleccionarTodos;

-(bool)addFila: (NSString*)nombre conLimiteLegal:(NSString*)limiteLegal;

-(void)deleteTodos;
-(DBResultSet *)filaConID: (NSNumber*)idNum;
-(void)actualizarFilaConID:(TipoAnalisis*)obj conNombre:(NSString*)nombre conLimiteLegal:(double)limiteLegal;
-(DBResultSet *)seleccionarTodosNombres;
+(NSString*)correctorPalabra: (NSString*)palabra;
-(NSNumber*)getId:(NSString*)nombre;
-(NSString*)getTipoAnalisis:(NSNumber*)identificador;
-(DBResultSet*)getObjetoConNombre:(NSString*)nombre;
-(DBResultSet*)getObjetoConId:(NSNumber*)identificador;
-(double)getLimiteLegal:(NSNumber*)idNum;
@end
