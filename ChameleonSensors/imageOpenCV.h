//
//  mathOpenCV.h
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 28/10/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "openCV.h"

@interface imageOpenCV : NSObject 

- (id)initAnalisis:(UIImage*)imagen;

-(double)getColorR;
-(double)getColorG;
-(double)getColorB;
-(void)setColorR:(double)colorR;
-(void)setColorG:(double)colorG;
-(void)setColorB:(double)colorB;
-(void)setMFe:(float)MFe;
-(void)setPpmFe:(float)PpmFe;
-(float)getMFe;
-(float)getPpmFe;
-(UIImage*)getImagenConCirculos;
-(void)setImagenConCirculos:(UIImage*)imagen;

-(NSMutableArray*)getDataDesconocida;
-(NSMutableArray*)getDataMuestras;
-(void)setDataDesconocida:(NSMutableArray*)data;
-(void)setDataMuestras:(NSMutableArray*)data;
-(int)getError;
-(void)setRectaAjuste:(NSMutableArray*)data;
-(NSMutableArray*)getRectaAjuste;


@end

