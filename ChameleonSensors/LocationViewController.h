//
//  LocationViewController.h
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 30/12/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface LocationViewController : UIViewController <CLLocationManagerDelegate,MKMapViewDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *mapa;

@property(nonatomic) float longitud;
@property(nonatomic) float latitud;

@end