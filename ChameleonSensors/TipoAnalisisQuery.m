//
//  BaseDatosAcceso.m
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 14/12/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//


#import "TipoAnalisisQuery.h"



@implementation TipoAnalisisQuery


/**
 * @brief Constructor TipoAnalisisQuery
 * @param (NSString *)dbFilename nombre de la base de datos
 * @return instancetype
 */
-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename{
    self = [super init];

    [DBAccess setDelegate:self];
    [DBAccess openDatabaseNamed:dbFilename];
    
    return self;
}


/**
 * @brief Función que elimina todo Tipoanálisis
 */
-(void)deleteTodos
{
    DBResultSet* rsTodos = [[TipoAnalisis query] fetch];
    
    [rsTodos removeAll];
    
}

/**
 * @brief Función que elimina un Tipoanálisis por id
 * @param (NSNumber*)idAnalisis
 */
-(void)elminarConID: (NSNumber*)idAnalisis{
    NSLog(@"id:\n%@",idAnalisis);
    DBResultSet* rsAnalisisId = [[[TipoAnalisis query]
                                  whereWithFormat:@"Id=%@d", idAnalisis]
                                 fetch];
    [rsAnalisisId removeAll];
    
    
}


/**
 * @brief Función que poner mayuscula la primera letra de la palabra
 * @param (NSString*)palabra
 * @return (NSString*) nueva palabra
 */
+(NSString*)correctorPalabra: (NSString*)palabra
{

    
    NSString *input = palabra;
    
    //objtenemos el primer carácter
    NSString *firstChar = [input substringToIndex:1];
    
    
    //se crea la palabra final con la primera letra mayuscula y el resto minusculas
    NSString *result = [[firstChar uppercaseString] stringByAppendingString:[input substringFromIndex:1]];
    
    return result;
}



/**
 * @brief Función que añade un Tipoanálisis
 * @param (NSString*)nombre
 * @param (NSString*)limiteLegal

 */
-(bool)addFila: (NSString*)nombre conLimiteLegal:(NSString*)limiteLegal
{
    
    if([limiteLegal containsString:@","]){
        limiteLegal = [limiteLegal stringByReplacingOccurrencesOfString:@"," withString:@"."];
    }
    
    
    
    
    nombre = [[self class] correctorPalabra:nombre];
    //si la palabra no esta incluida en la BD, se añade
    if(![self.seleccionarTodosNombres containsObject:nombre]){
        TipoAnalisis* tipoAnalisis = [TipoAnalisis new];
        tipoAnalisis.nombre=nombre;
        tipoAnalisis.limiteLegal=[limiteLegal doubleValue];
        
        
        // save the object into the table
        [tipoAnalisis commit];
        
        return true;
    }
    
    return false;
    
    
}

/**
 * @brief Función devuelve en un resultSet todos Tipoanálisis
 * @return (DBResultSet*)tipoanálisis
 */
-(DBResultSet *)seleccionarTodos{
    
    DBResultSet* rsTodos = [[TipoAnalisis query] fetch];
    
    return rsTodos;
}

/**
 * @brief Función devuelve todos los nombrs de los tipos de análisis guardados
 * @return (NSArray*)array con todos los nombres
 */
-(NSArray *)seleccionarTodosNombres{
    
    NSDictionary* rsTodos = [[TipoAnalisis query] groupBy:@"nombre"];
    
    NSArray *result =[rsTodos allKeys];
    
    if(result.count ==0){
        return @[NSLocalizedString(@"Ninguno", nil)];
    }
   
    return result;
}


/**
 * @brief Función devuelve el Tipoanálisis que coincide con el identificador
 * @param (NSNumber*)identificador
 * @return (DBResultSet*)
 */
-(NSString*)getTipoAnalisis:(NSNumber*)identificador
{
    DBResultSet* tipoAnalisis = [[[[TipoAnalisis query] limit:1]
                                 whereWithFormat:@"Id = %@", identificador]
                                fetch];
    
    TipoAnalisis* jew = [tipoAnalisis objectAtIndex:0];
    
    return [jew nombre];
    
}

/**
 * @brief Función devuelve el objeto que coincide en el nombre
 * @param (NSString*)nombre
 * @return (DBResultSet*)
 */
-(DBResultSet*)getObjetoConNombre:(NSString*)nombre
{
    DBResultSet* tipoAnalisis = [[[[TipoAnalisis query] limit:1]
                                  whereWithFormat:@"nombre = %@", nombre]
                                 fetch];
    
    return tipoAnalisis;
    
}

/**
 * @brief Función devuelve el objeto que coincide en el identificador
 * @param (NSNumber*)identificador
 * @return (DBResultSet*)
 */
-(DBResultSet*)getObjetoConId:(NSNumber*)identificador
{
    DBResultSet* tipoAnalisis = [[[[TipoAnalisis query] limit:1]
                                  whereWithFormat:@"Id = %@", identificador]
                                 fetch];
    
    return tipoAnalisis;
    
}

/**
 * @brief Función devuelve el identificador que coincide en el nombre
 * @param (NSString*)nombre
 * @return (NSNumber*)
 */
-(NSNumber*)getId:(NSString*)nombre
{
    DBResultSet* limiteLegal = [[[[TipoAnalisis query] limit:1]
                       whereWithFormat:@"nombre = %@", nombre]
                      fetch];
    
    TipoAnalisis* jew = [limiteLegal objectAtIndex:0];
    
    return [jew Id];
    
}

/**
 * @brief Función devuelve el limite legal del objeto que tenga el identificador
 * @param (NSNumber*)idNum
 * @return (double*)
 */
-(double)getLimiteLegal:(NSNumber*)idNum
{
    DBResultSet* limiteLegal = [[[[TipoAnalisis query] limit:1]
                                 whereWithFormat:@"Id = %@", idNum]
                                fetch];
    
    TipoAnalisis* jew = [limiteLegal objectAtIndex:0];
    
    return [jew limiteLegal];
    
}


/**
 * @brief Función establece nuevo nombre a un Tipoanálisis
 * @param (NSNumber*)idNum
 * @param (NSString*)nombre
 */
-(void)actualizarNombreConID: (NSNumber*)idNum conNombre:(NSString*)nombre {
    DBResultSet* r = [[[[TipoAnalisis query]
                        whereWithFormat:@"Id=%d",  idNum]
                       limit:1]
                      fetch];
    TipoAnalisis* jew = [r objectAtIndex:0];
    [jew setNombre:nombre];
    [jew commit];
    
}

/**
 * @brief Función establece nuevo limite legal a un Tipoanálisis
 * @param (NSNumber*)idNum
 * @param (double)limiteLegal
 */
-(void)actualizarLimiteLegalConID: (NSNumber*)idNum conLimiteLegal:(double)limiteLegal {
    DBResultSet* r = [[[[TipoAnalisis query]
                        whereWithFormat:@"Id=%d",  idNum]
                       limit:1]
                      fetch];
    TipoAnalisis* jew = [r objectAtIndex:0];
    [jew setLimiteLegal:limiteLegal];
    [jew commit];
    
}

/**
 * @brief Función actualiza datos objeto Tipoanalisis
 * @param (TipoAnalisis*)obj
 * @param (NSString*)nombre
 * @param (double)limiteLegal
 */
-(void)actualizarFilaConID:(TipoAnalisis*)obj conNombre:(NSString*)nombre conLimiteLegal:(double)limiteLegal{

    [obj setLimiteLegal:limiteLegal];
    [obj setNombre:nombre];
    [obj commit];
    
}


/**
 * @brief Función devuelve objeto Tipoanalisis
 * @param (NSNumber*)idNum
 * @return DBResultSet
 */
-(DBResultSet *)filaConID: (NSNumber*)idNum{
    NSLog(@"id:\n%@",idNum);
    DBResultSet* r = [[[TipoAnalisis query]
                       whereWithFormat:@"Id=%d",  idNum]
                      fetch];
    return r;
}





@end