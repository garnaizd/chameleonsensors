//
//  analisis.m
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 10/12/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import "Analisis.h"



@implementation Analisis

@dynamic colorR,colorG,colorB,MFe,PpmFe,DataMuestras,DataDesconocida,UrlImagen,Observacion,Fecha,latitud,DataAjuste,longitud,titulo,idTipoAnalisis;


@end

