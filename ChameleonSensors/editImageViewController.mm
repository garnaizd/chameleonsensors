//
//  editImageViewController.m
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 14/10/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import "editImageViewController.h"
#import "imageOpenCV.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "AnalisisQuery.h"
#import "TipoAnalisisQuery.h"
#import "UIAlertController.h"
#import "LocationViewController.h"

@interface editImageViewController()


///&lt; imagen capturada con la cámara o seleccionada de la galería
@property (weak, nonatomic) IBOutlet UIImageView *imgSoloCirculos;
///&lt; label donde aparece el color rojo detectado de la muestra desconocida
@property (weak, nonatomic) IBOutlet UITextField *txtR;
///&lt; label donde aparece el color verde detectado de la muestra desconocida
@property (weak, nonatomic) IBOutlet UITextField *txtG;
///&lt; label donde aparece el color azul detectado de la muestra desconocida
@property (weak, nonatomic) IBOutlet UITextField *txtB;

///&lt; imagen con el color de la muestra desconocida
@property (weak, nonatomic) IBOutlet UIImageView *imgMuestraDesconocida;
///&lt; label mFe
@property (weak, nonatomic) IBOutlet UITextField *mFe;
///&lt; label partes por millón de la concentración
@property (weak, nonatomic) IBOutlet UITextField *ppmFe;
///&lt; label tipo de análisis asociado
@property (weak, nonatomic) IBOutlet UITextField *tipo;
///&lt; label límite legal del tipo de análisis asociado
@property (weak, nonatomic) IBOutlet UITextField *limiteLegal;

///&lt; imagen legal/ilegal
@property (weak, nonatomic) IBOutlet UIImageView *imageLegalIlegal;


///&lt; imagen donde aparece el gráfico con las partes por millón
@property (weak, nonatomic) IBOutlet UIImageView *imgGraficaPorcentaje;
///&lt; Botón que muestra la gráfica generada
@property (weak, nonatomic) IBOutlet UIButton *btnGrafica;
///&lt; Botón que muestra el mapa con la posición gps almacenada
@property (weak, nonatomic) IBOutlet UIButton *btnVerMapa;


///&lt; Objeto analisisQuery
@property (nonatomic, strong) AnalisisQuery *dbManager;
///&lt; Objeto TipoAnalisisQuery
@property (nonatomic, strong) TipoAnalisisQuery *dbManagerTipoAnalisis;
///&lt; Botón navigatiónControlle editar
@property (strong, nonatomic) IBOutlet UIBarButtonItem *editarButton;
///&lt; Botón navigatiónControlle guardar
@property (strong, nonatomic) IBOutlet UIBarButtonItem *guardarButton;


//opciones de edicion
///&lt; vista de la sección observación
@property (weak, nonatomic) IBOutlet UIImageView *imgComentario;
///&lt; textView sección observación
@property (weak, nonatomic) IBOutlet UITextView *txtFieldObservacion;
///&lt; Array la url de la imagen
@property (nonatomic) NSMutableArray *imgFilePath;
///&lt; Icono gps de la vista
@property (weak, nonatomic) IBOutlet UIButton *gpsIcon;

///&lt; label con el texto de legal/ilegal
@property (weak, nonatomic) IBOutlet UILabel *txtLegalIlegal;


///&lt; Vista global
@property (strong, nonatomic) IBOutlet UIView *viewGlobal;

///&lt; Vista composición
@property (weak, nonatomic) IBOutlet UIView *viewComposicion;
///&lt; Vista RGB
@property (weak, nonatomic) IBOutlet UIView *viewRGB;

///&lt; Vista limite legal
@property (weak, nonatomic) IBOutlet UIView *viewLimiteLegal;

//view grafica
///&lt; Vista gráfica
@property (weak, nonatomic) IBOutlet UIView *viewGrafica;

///&lt; Vista ScrollView
@property (weak, nonatomic) IBOutlet UIView *viewDentroScrollView;

///&lt; Vista Observaciones
@property (weak, nonatomic) IBOutlet UIView *viewObservaciones;

-(bool)esLegal:(float)ppmFe;
-(void)guardarImagenEnCarpeta;
-(void)CerrarActualNaviationController;
-(CLLocationCoordinate2D) getCurrentLocation;
-(void) inicializarInterfaz;
-(NSString*)saveImagen:(UIImage*)imagen;
- (void)mostrarEditarToolbar;
-(void) guardarSQLite;
@end

@implementation editImageViewController
@synthesize BMIChart,longitud,latitud;

//pop over with textview
UIView *alertaView;
CustomIOSAlertView *alertViewIOS;

using namespace cv;
using namespace std;



//Variables globales
NSString *observacion;
NSString* nombreAnalisis = @"";
NSUserDefaults *defaultsUser = [NSUserDefaults standardUserDefaults];

imageOpenCV *imagenOpenCV;

//se ejecuta por detras

/**
 * @brief Función que se ejecita en cada inicio de la vista
 */
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addScrollView];
    [_btnGrafica setTitle:NSLocalizedString(@"Ver gráfica", nil) forState:UIControlStateNormal];
    [_btnVerMapa setTitle:NSLocalizedString(@"Ver mapa", nil) forState:UIControlStateNormal];
    
    UIBarButtonItem *botonAtras = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Atrás", nil) style:UIBarButtonItemStylePlain target:self action:@selector(CerrarActualNaviationController)];
    
    self.navigationItem.leftBarButtonItem = botonAtras;
    
    self.dbManager = [[AnalisisQuery alloc]
                      initWithDatabaseFilename:@"ChameleonSensorsDB"];
    
    self.dbManagerTipoAnalisis = [[TipoAnalisisQuery alloc]
                                  initWithDatabaseFilename:@"ChameleonSensorsDB"];
    [self generarAnalisis];
    
    
    self.imgFilePath = [[NSMutableArray alloc] init];
    
    self.title = NSLocalizedString(@"Analisis", nil);
    
    
    CLLocationCoordinate2D coordinate = [self getCurrentLocation];

    latitud = coordinate.latitude ;
    longitud = coordinate.longitude;
    NSLog(@"*dLatitude : %f", longitud);
    NSLog(@"*dLongitude : %f",latitud);
}

/**
 * @brief Función que ejecuta la transación entre la actual vista y la vista del mapa
 * @param (id)sender
 * @return IBAction acción
 */
- (IBAction)mostrarMapa:(id)sender {
    [self performSegueWithIdentifier:@"mapSegue" sender:nil];
}

/**
 * @brief Función que genera el informe
 */
-(void)generarAnalisis
{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Calculando";
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.02 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        //editar
        if (self.objetoAnalisis != nil) {
            self.title = self.objetoAnalisis.titulo;
            [self loadInfoToEdit];
            
            //nuevo analisis
        }else{
            imagenOpenCV=[[imageOpenCV alloc]initAnalisis:self.imagen];
            
            if(imagenOpenCV.getError == 1){
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Aviso", nil)
                                                                                         message:[NSString stringWithFormat:NSLocalizedString(@"Se ha producido un error en el reconocimiento de la muestra, realice otra foto", nil)]
                                                                                  preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:0] animated:YES];
                }];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            
            }else{
                [self inicializarInterfaz];
            }
        }
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        
    });
    
    
}






/**
 * @brief Se ejecuta cuando se cierra la vista actual
 */
-(void)CerrarActualNaviationController
{
    if (self.objetoAnalisis == nil){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Aviso", nil)
                                                                             message:[NSString stringWithFormat:NSLocalizedString(@"¿Seguro que desea descartar este análisis?", nil)]
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
        UIAlertAction* no = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancelar", nil) style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:no];
    
        UIAlertAction* si = [UIAlertAction actionWithTitle:NSLocalizedString(@"Si", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        [self.navigationController popViewControllerAnimated:YES];
        
        }];
   
        [alertController addAction:si];
    
 
    [self presentViewController:alertController animated:YES completion:nil];
        
    }else{
        
        [self.navigationController popViewControllerAnimated:YES];
    }
   
    
}


/**
 * @brief Función que ejecuta cuando la actual vista desaparece
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    

    
    if (self.objetoAnalisis != nil) {
        
        [self mostrarEditarToolbar];
        self.viewObservaciones.hidden = false;

        
    }else{
        [self mostrarGuardarToolbar];
        self.viewObservaciones.hidden = true;
      
    }

    
    
}




/**
 * @brief Función que obtiene la posición actual del usuario
 * @return CLLocationCoordinate2D posición 2D
 */
-(CLLocationCoordinate2D) getCurrentLocation
{
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    
    return coordinate;
}

/**
 * @brief Función que inicializa la interfaz con los datos de cada campo
 */
-(void) inicializarInterfaz
{
    NSLog(@"%@",imagenOpenCV);
    //imagen
    _imgSoloCirculos.image = imagenOpenCV.getImagenConCirculos;
    
    //RGB
    _txtR.text =[NSString stringWithFormat:@"%0.2f",imagenOpenCV.getColorR];
    _txtG.text =[NSString stringWithFormat:@"%0.2f",imagenOpenCV.getColorG];
    _txtB.text =[NSString stringWithFormat:@"%0.2f",imagenOpenCV.getColorB];
    
    
    
    //color muestra desconocida
    _imgMuestraDesconocida.backgroundColor = [[UIColor alloc] initWithRed:(imagenOpenCV.getColorR)/255. green:( imagenOpenCV.getColorG)/255. blue:(imagenOpenCV.getColorB)/255. alpha:1.0];
    
    //concentración estimada
   // _concentracionEstimada.text =[NSString stringWithFormat:@"%0.2f",yTEST];
    _ppmFe.text = [NSString stringWithFormat:@"%.07f" ,imagenOpenCV.getPpmFe];
    _mFe.text = [NSString stringWithFormat:@"%.07f" ,imagenOpenCV.getMFe];
   
    
    self.tipo.text = _objetoTipoAnalisis.nombre;
    self.limiteLegal.text = [NSString stringWithFormat:@"%0.2f",_objetoTipoAnalisis.limiteLegal];
    
    
    

    [self generarGraficaPorcentaje:100 minimo:imagenOpenCV.getPpmFe];
    
    
    if([self esLegal:imagenOpenCV.getPpmFe])
    {
        _imageLegalIlegal.image = [UIImage imageNamed:@"checkicon"];
        _txtLegalIlegal.text =NSLocalizedString(@"Legal", nil);
    }else{
         _imageLegalIlegal.image = [UIImage imageNamed:@"uncheckicon"];
        _txtLegalIlegal.text =NSLocalizedString(@"Ilegal", nil);
    }
    
}

/**
 * @brief Función que genera el gráfico que muestra las partes por millón de hierro
 * @param (int)max valor máximo de la gráfica
 * @param (float)min valor mínimo de la gráfica
 */
-(void) generarGraficaPorcentaje:(int)max minimo:(float)min
{
    BMIChart = [[PNCircleChart alloc] initWithFrame: _imgGraficaPorcentaje.frame total:@(max) current:@(min) clockwise:YES shadow:YES shadowColor:PNLightBlue displayCountingLabel:YES];
    
                BMIChart.chartType=PNChartFormatTypeDecimal;
 
               BMIChart.backgroundColor = [UIColor clearColor];
               [BMIChart setStrokeColor:PNLightGreen];
               [BMIChart strokeChart];
               BMIChart.userInteractionEnabled = YES;
    
    
   [self.viewComposicion addSubview:self.BMIChart];
}

/**
 * @brief Función que edita el análisis y guarda los cambios
 * @param (id)sender
 * @return IBAction acción
 */
-(IBAction)editarAnalisis:(id)sender
{
    
    UIView *vista = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 240)];
    
    
    UILabel *txtTitulo = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 270, 25)];
    txtTitulo.text = NSLocalizedString(@"Titulo", nil);
    [txtTitulo setFont:[UIFont boldSystemFontOfSize:16]];
    [vista addSubview:txtTitulo];
    
    UITextField *textFieldTitulo = [[UITextField alloc] initWithFrame:CGRectMake(10, 25, 270, 30)];
    textFieldTitulo.text =self.objetoAnalisis.titulo;
    textFieldTitulo.borderStyle = UITextBorderStyleRoundedRect;
    textFieldTitulo.backgroundColor= [UIColor whiteColor];
    
    [vista addSubview:textFieldTitulo];
    
    UILabel *txtObservacion = [[UILabel alloc] initWithFrame:CGRectMake(10, 55, 270, 25)];
    txtObservacion.text = NSLocalizedString(@"Observación", nil);
    [txtObservacion setFont:[UIFont boldSystemFontOfSize:16]];
    
    [vista addSubview:txtObservacion];
    
    UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(10, 80,270,150)];
    textView.editable = true;
    textView.text=self.objetoAnalisis.Observacion;
    [textView setFont:[UIFont systemFontOfSize:14]];
    [vista addSubview:textView];
    
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    alertView.backgroundColor = [UIColor colorWithRed:236.f green:240.f blue:241.f alpha:0.4f];
    
    [alertView setContainerView:vista];
    
    
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Cancelar",@"Guardar", nil]];
    [alertView setDelegate:self];
    
    
    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        
        if( buttonIndex ==1){
            observacion = textView.text;
            
            
            [self.dbManager setTituloID:self.objetoAnalisis conTitulo:textFieldTitulo.text];
            
            [self.dbManager setObservacionID:self.objetoAnalisis conObervacion:textView.text];
            
            [self.delegate editingInfoWasFinished];
            
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        [alertView close];
        
    }];
    
    [alertView setUseMotionEffects:TRUE];
    
    [alertView show];
    
    [self.view addSubview:alertView];
}


/**
 * @brief Función que muestra/esconde botones en función de la acción del usuario
 */
- (void)mostrarEditarToolbar
{
    [self.navigationItem setRightBarButtonItems:@[self.editarButton] animated:YES];
    
    
    [_txtFieldObservacion setHidden:NO];
    
    NSLog(@"%@",[defaultsUser objectForKey:@"gpsSettings"]);
    
    if (_objetoAnalisis.latitud!=0 || _objetoAnalisis.longitud!=0){
        [_gpsIcon setHidden:NO];
        [_btnVerMapa setHidden:NO];
    }
    
    
}

/**
 * @brief Función que compara el valor ppmFe con el límite legal del tipo de análisis. Si el valor ppmFe es mayor que el límite legal se considera ilegal el análisis.
 * @param (float)ppmFe concentración del análisis
 * @return bool que indica si es legal o no.
 */
-(bool)esLegal:(float)ppmFe
{
    if(ppmFe >_objetoTipoAnalisis.limiteLegal )
    {
        return false;
    }
    return true;
    
}

/**
 * @brief Función que muestra el botón guardar en el NavigationController
 */
- (void)mostrarGuardarToolbar {
    [self.navigationItem setRightBarButtonItems:@[self.guardarButton] animated:YES];
}

/**
 * @brief Función que guarda el titulo y observación asociado al análisis
 * @param (id)sender
 * @return IBAction acción que se ejecuta cuando se pulsa el botón guardar
 */
- (IBAction)guardarAnalisis:(id)sender {

    UIView *vista = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 240)];
    
    
    UILabel *txtTitulo = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 270, 25)];
    txtTitulo.text = NSLocalizedString(@"Titulo analisis", nil);
    [txtTitulo setFont:[UIFont boldSystemFontOfSize:16]];
    [vista addSubview:txtTitulo];
    
    UITextField *textFieldTitulo = [[UITextField alloc] initWithFrame:CGRectMake(10, 25, 270, 30)];
    textFieldTitulo.borderStyle = UITextBorderStyleRoundedRect;
    textFieldTitulo.backgroundColor= [UIColor whiteColor];
    
    [vista addSubview:textFieldTitulo];
    
    UILabel *txtObservacion = [[UILabel alloc] initWithFrame:CGRectMake(10, 55, 270, 25)];
    txtObservacion.text = NSLocalizedString(@"Observación", nil);
    [txtObservacion setFont:[UIFont boldSystemFontOfSize:16]];
    
    [vista addSubview:txtObservacion];
    
    UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(10, 80,270,150)];
    textView.editable = true;
    [textView setFont:[UIFont systemFontOfSize:14]];
    [vista addSubview:textView];
    
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    alertView.backgroundColor = [UIColor colorWithRed:236.f green:240.f blue:241.f alpha:0.4f];
    
    [alertView setContainerView:vista];
    
    
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Cancelar",@"Guardar", nil]];
    [alertView setDelegate:self];
    
    
    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        
        if( buttonIndex ==1){
            nombreAnalisis =textFieldTitulo.text;
            observacion = textView.text;
            [self guardarSQLite];
        }
        [alertView close];
        
    }];
    
    [alertView setUseMotionEffects:TRUE];
    
    [alertView show];
    
    [self.view addSubview:alertView];
}




# pragma mark - Popover Delegates

/**
 * @brief Delegate incio alerta
 * @param (CustomIOSAlertView *)alertView  vista de la alerta
 * @param (NSInteger)buttonIndex indice del botón pulsado
 */
- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Button at position %ld is clicked on alertView %ld.", (long)buttonIndex, (long)[alertView tag]);
}

/**
 * @brief Delegate cierre alerta
 * @param (UIPopoverPresentationController *)popoverPresentationController  vista de la alerta
 */
- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    
    NSLog(@"Popover was dismissed with external tap. Have a nice day!");
}

/**
 * @brief Delegate que cierre alerta
 * @param (UIPopoverPresentationController *)popoverPresentationController  vista de la alerta
 */
- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    
 
    return YES;
}

/**
 * @brief Delegate que presentación alerta
 * @param (UIPopoverPresentationController *)popoverPresentationController  vista de la alerta
 */
- (void)popoverPresentationController:(UIPopoverPresentationController *)popoverPresentationController willRepositionPopoverToRect:(inout CGRect *)rect inView:(inout UIView *__autoreleasing  _Nonnull *)view {
    
}




/**
 * @brief Función que obtiene la fecha actual del dispositivo
 * @param (NSDateFormatter *) fecha obtenida
 */
- (NSDateFormatter*)obtenerFechaString
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    
    return dateFormatter;
    
}

/**
 * @brief Función que crea carpeta ChameleonSensors en caso de que no exista ya en el dispositivo
 */
-(void)guardarImagenEnCarpeta
{
    
    __block PHObjectPlaceholder *placeholder = nil;
    //guardamos en el directorio chameleon
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        PHAssetChangeRequest *assetChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:self.imagen];
        
        if (self.assetCollection) {
            PHAssetCollectionChangeRequest *assetCollectionChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:self.assetCollection];
            [assetCollectionChangeRequest addAssets:@[[assetChangeRequest placeholderForCreatedAsset]]];
            
            
            
            placeholder = assetChangeRequest.placeholderForCreatedAsset;
            
            [assetCollectionChangeRequest addAssets:@[placeholder]];
            
        }
        
    } completionHandler:^(BOOL success, NSError *error) {
        
    }];
}


/**
 * @brief Función que guarda la imagen capturada con el análisis
 * @param (UIImage*)imagen imagen que se quiere guardar
 * @return NSString* ruta en la que se ha guardado la imagen
 */
-(NSString*)saveImagen:(UIImage*)imagen
{
    [self guardarImagenEnCarpeta];
    
    
    NSData *pngData = UIImagePNGRepresentation(imagen);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
   NSString* fechaString = [[[self obtenerFechaString] stringFromDate:[NSDate date]] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    
    NSString *imgFile = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@",fechaString,@".png"]];
    [pngData writeToFile:imgFile atomically:YES]; //Write the file
    [self.imgFilePath addObject:imgFile];
    
    return imgFile;
}

/**
 * @brief Función que guarda el análisis en base de datos
 */
-(void) guardarSQLite
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *tipoAnalisis = [defaults objectForKey:@"tipoAnalisis"];
    
    NSNumber* idTipoAnalisis = [self.dbManagerTipoAnalisis getId:tipoAnalisis];
    

    NSLog(@"%@",idTipoAnalisis);
    
    
    NSString *ruta = [self saveImagen:self.imagen];
    if([nombreAnalisis isEqual:@""]){
   
    int numAnalisis = [[defaults objectForKey:@"numAnalisis"] intValue];
    if([self.dbManager getLastId] ==nil){
        [defaults setObject:[NSNumber numberWithInt:(numAnalisis +1)] forKey:@"numAnalisis"];
        
        numAnalisis = numAnalisis +1;
        nombreAnalisis =[NSString stringWithFormat:@"%@ %d",NSLocalizedString(@"Analisis",nil),numAnalisis];
        
    }else{
        int numero = [[[self.dbManager getLastId] valueForKey:@"Id"] intValue];
        if(numero +1 > numAnalisis){
         [defaults setObject:[NSNumber numberWithInt:numero+1] forKey:@"numAnalisis"];
        }
        numero = numero+1;
        nombreAnalisis = [NSString stringWithFormat:@"%@ %d",NSLocalizedString(@"Analisis",nil),numero];
    }
    }
    
    [self.dbManager addFila:imagenOpenCV.getColorR conColorG:imagenOpenCV.getColorG conColorB:imagenOpenCV.getColorB conMFe:imagenOpenCV.getMFe conPpmFe:imagenOpenCV.getPpmFe conDataMuestras:imagenOpenCV.getDataMuestras conDataDesconocida:imagenOpenCV.getDataDesconocida conDataAjuste:imagenOpenCV.getRectaAjuste conUrlImagen:ruta conObservacion:observacion conFecha:self.fechaCreacion conLongitud:longitud conLatitud:latitud conTitulo:nombreAnalisis conIdTipoAnalisis:idTipoAnalisis];
    
   
    
    
        [self.delegate editingInfoWasFinished];
        
        [[self navigationController] popViewControllerAnimated:YES];
        

}

/**
 * @brief Función que genera la transición entre la actual vista y la vista de la gráfica
 * @param (id)sender
 * @return IBAction acción de transición
 */
- (IBAction)mostrarGrafica:(id)sender {
    [self performSegueWithIdentifier:@"corePlotSegue" sender:nil];
    
}





/**
 * @brief Función que pasa datos entre diferentes vista en función del segue(vista final)
 * @param (UIStoryboardSegue *)segue vista final
 * @param (id)sender
 */
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    

    if([segue.identifier isEqualToString:@"corePlotSegue"]){
        [corePlotScatter setyAxisMin:-1];
        [corePlotScatter setyAxisMax:68];
        
        [corePlotScatter setxAxisMin:-3];
        [corePlotScatter setxAxisMax:3];
        
        corePlotScatter *corePlot = (corePlotScatter *)segue.destinationViewController;
        
        
        if (self.objetoAnalisis != nil) {
            corePlot.graphData = _objetoAnalisis.DataMuestras;
            corePlot.graphData2 =_objetoAnalisis.DataDesconocida;
            corePlot.graphRectaAjuste =_objetoAnalisis.DataAjuste;
        
        
        }else{
            corePlot.graphData = imagenOpenCV.getDataMuestras;
            corePlot.graphData2 =imagenOpenCV.getDataDesconocida;
            corePlot.graphRectaAjuste =imagenOpenCV.getRectaAjuste;
        
        }
    }else if([segue.identifier isEqualToString:@"mapSegue"]){
        LocationViewController * location = (LocationViewController *)segue.destinationViewController;
        location.longitud = longitud;
        location.latitud = latitud;
        
    }
    
    
    
    
    
    
}


/**
 * @brief Función que obtiene una imagen del dispositivo a partir de una url
 * @param (NSString*)urlImagen direción donde está almacenada la imagen
 * @return UIImage* imagen obtenida
 */
-(UIImage*) getImagenUrl:(NSString*)urlImagen
{

    UIImage *imagen = [UIImage imageWithContentsOfFile:urlImagen];
    
    return imagen;
}


/**
 * @brief Función que muestra los datos del análisis seleccionado
 */
-(void)loadInfoToEdit{
    NSLog(@"%@",_objetoAnalisis);
    self.imgSoloCirculos.image = [self getImagenUrl:_objetoAnalisis.UrlImagen];
    self.txtR.text = [NSString stringWithFormat:@"%.02f" , _objetoAnalisis.colorR];
    self.txtG.text = [NSString stringWithFormat:@"%.02f" , _objetoAnalisis.colorG];
    self.txtB.text = [NSString stringWithFormat:@"%.02f" , _objetoAnalisis.colorB];
    
    
    self.imgMuestraDesconocida.backgroundColor = [[UIColor alloc] initWithRed:(_objetoAnalisis.colorR)/255. green:(_objetoAnalisis.colorG)/255. blue:(_objetoAnalisis.colorB)/255. alpha:1.0];
    
    
    self.mFe.text= [NSString stringWithFormat:@"%.07f" ,_objetoAnalisis.MFe];
    self.ppmFe.text= [NSString stringWithFormat:@"%.03f" ,_objetoAnalisis.PpmFe];
    self.tipo.text= _objetoTipoAnalisis.nombre;
    self.limiteLegal.text = [NSString stringWithFormat:@"%.02f" ,_objetoTipoAnalisis.limiteLegal];
    //self.comentario =_objetoAnalisis.Observacion;
    self.txtFieldObservacion.text = _objetoAnalisis.Observacion;
   // self.imageLegalIlegal= @(_objetoAnalisis.colorB).stringValue;
    
    [self generarGraficaPorcentaje:100 minimo:_objetoAnalisis.PpmFe];

    
    if([self esLegal:_objetoAnalisis.PpmFe])
    {
        _imageLegalIlegal.image = [UIImage imageNamed:@"checkicon"];
        _txtLegalIlegal.text =NSLocalizedString(@"Legal", nil);
    }else{
        _imageLegalIlegal.image = [UIImage imageNamed:@"uncheckicon"];
        _txtLegalIlegal.text =NSLocalizedString(@"Ilegal", nil);
    }
}



/**
 * @brief Función que muestra u oculta vistas del scrollview en función si se está editando un análisis o guardando
 */
-(void)addScrollView{

    [_scrollView setScrollEnabled:YES];
    
    if (self.objetoAnalisis != nil) {
    [_scrollView setContentSize:CGSizeMake(self.viewDentroScrollView.bounds.size.width, self.viewDentroScrollView.bounds.size.height + self.navigationController.navigationBar.frame.size.height)];
        
    }else{
        
        [_scrollView setContentSize:CGSizeMake(self.viewDentroScrollView.bounds.size.width, self.viewDentroScrollView.bounds.size.height + self.navigationController.navigationBar.frame.size.height - self.viewObservaciones.frame.size.height)];
        
    }
}

/**
 * @brief Función que permite la edición
 * @param (NSSet *)touches
 * @param (UIEvent *)event
 */
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}


@end
