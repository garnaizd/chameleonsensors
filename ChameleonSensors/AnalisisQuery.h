//
//  BaseDatosAcceso.h
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 14/12/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "Analisis.h"

@interface AnalisisQuery : NSObject<DBDelegate>

@property (nonatomic, strong) NSString *documentsDirectory;
@property (nonatomic, strong) NSString *databaseFilename;


-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename;

-(void)elminarConID: (NSNumber*)idAnalisis;
-(DBResultSet *)seleccionarTodos;
-(void)actualizarConID: (int)idNum withAge:(int)age withFN:(NSString*)fn withLN:(NSString*)ln;


-(void)addFila: (double)colorR conColorG:(double)colorG conColorB:(double)colorB conMFe:(float)MFe conPpmFe:(float)PpmFe conDataMuestras:(NSMutableArray*)DataMuestras conDataDesconocida:(NSMutableArray*)DataDesconocida conDataAjuste:(NSMutableArray*)dataAjuste conUrlImagen:(NSString*)UrlImagen conObservacion:(NSString*)Observacion conFecha:(NSString*)Fecha conLongitud:(float)longitud conLatitud:(float)latitud conTitulo:(NSString*)titulo conIdTipoAnalisis:(NSNumber*)idTipoAnalisis;

-(void)addObservacion:(Analisis*)objeto newObservacion:(NSString*)observacion;

-(void)deleteTodos;
-(BOOL)getAnalisis:(NSNumber*)identificador;
-(NSArray*)getLastId;
-(void)setTituloID:(Analisis*)obj conTitulo:(NSString*)titulo;
-(void)setObservacionID:(Analisis*)obj conObervacion:(NSString*)observacion;
-(DBResultSet *)getAnalisisArray:(NSNumber*)identificador;
@end
