//
//  Settings.m
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 17/12/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//


#import "SettingsForm.h"
#import "TipoAnalisisQuery.h"


@interface SettingsForm (){
    TipoAnalisisQuery *dbManager;
 
}
@property (nonatomic, strong) NSArray *arrTipoAnalisis;

@end

@implementation SettingsForm

/**
 * @brief Función que inicializa la vista de Configuración
 * @param NSArray* con las celdas inicializadas
 */
- (NSArray *)fields
{
    
    
    TipoAnalisisQuery *dbManager;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    
    NSLog(@"gps %@",[defaults objectForKey:@"gpsSettings"]);
    
    NSLog(@"vibracion %@",[defaults objectForKey:@"vibracionSettings"]);
    
    dbManager = [[TipoAnalisisQuery alloc]
                 initWithDatabaseFilename:@"ChameleonSensorsDB"];
    
    
    

    
    return @[
             

             //seccion Configuracion sistema
             @{ FXFormFieldHeader:NSLocalizedString(@"Configuracion sistema", nil),
                FXFormFieldKey: @"gpsSwitch",
                FXFormFieldTitle:NSLocalizedString(@"Activar gps", nil),
                FXFormFieldDefaultValue:[defaults objectForKey:@"gpsSettings"],
                FXFormFieldAction:@"activarGps",
                FXFormFieldCell:[FXFormSwitchCell class]},
             

             
             @{FXFormFieldTitle:NSLocalizedString(@"Concentraciones muestras", nil),
               FXFormFieldHeader: NSLocalizedString(@"Ajustes", nil),
               FXFormFieldSegue:@"ConcentracionesSegue"},
             

             
             
             @{FXFormFieldKey:@"tipoAnalisis",
               FXFormFieldTitle:NSLocalizedString(@"Tipo Análisis seleccionado", nil),
               FXFormFieldOptions: dbManager.seleccionarTodosNombres,
               FXFormFieldAction:@"selectTipoAnalisis",
               FXFormFieldDefaultValue:[defaults objectForKey:@"tipoAnalisis"],
               FXFormFieldFooter:NSLocalizedString(@"Esta celda permite seleccionar el tipo de análisis por defecto para crear análsiis, puede ser cambiado tantas veces como desee. De esta forma puede tener diferentes análisis asignados a distintos tipos de análisis.", nil)},
     

             
             @{FXFormFieldTitle:NSLocalizedString(@"Borrar historial", nil) , FXFormFieldHeader: @"", FXFormFieldAction: @"submitBorrarHistorial:",
               FXFormFieldFooter:NSLocalizedString(@"Al pulsar Borrar historial, borra todos los análisis que ha realizado hasta el momento", nil)},
             
             
             
             @{FXFormFieldTitle:NSLocalizedString(@"Autor", nil),
               FXFormFieldHeader: NSLocalizedString(@"Otros", nil),
               FXFormFieldPlaceholder:@"Gema Arnaiz Delgado",
               },
             
             @{FXFormFieldTitle:NSLocalizedString(@"Versión", nil),
               FXFormFieldPlaceholder:@"1.0",
               },
             
                
             ];
}



@end
