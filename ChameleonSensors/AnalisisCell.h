//
//  AnalisisCell.h
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 15/12/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnalasisCell : UITableViewCell<UITableViewDelegate>

///&lt;  imagen del sensor
@property (nonatomic, strong) IBOutlet UIImageView *ImagenCell;
///&lt;  titulo del análisis
@property (nonatomic, strong) IBOutlet UILabel *TituloCell;
///&lt;  concentración del análisis
@property (nonatomic, strong) IBOutlet UILabel *ConcentracionCell;
///&lt;  fecha cuando fue realizado el análisis
@property (nonatomic, strong) IBOutlet UILabel *FechaCell;
///&lt;  tipo de análisis asociado al análisis
@property (weak, nonatomic) IBOutlet UILabel *tipoAnalisis;
///&lt;  imagen legal/ilegal
@property (nonatomic, strong) IBOutlet UIImageView *ImagenLegalCell;
///&lt;  texto legal/ilegal
@property (nonatomic, strong) IBOutlet UILabel *txtlegalIlegal;
@end