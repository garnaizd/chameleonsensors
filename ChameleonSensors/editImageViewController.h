//
//  editImageViewController.h
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 14/10/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <QuartzCore/QuartzCore.h>
#import "corePlotScatter.h"
#import "PNCircleChart.h"
#import "Analisis.h"
#import <Photos/Photos.h>
#import "IOSAlertView.h"
#import <CoreLocation/CoreLocation.h>

@protocol editImageViewControllerDelegate

-(void)editingInfoWasFinished;

@end




@interface editImageViewController : UIViewController<UIScrollViewDelegate,UIPopoverPresentationControllerDelegate,CustomIOSAlertViewDelegate,CLLocationManagerDelegate>

@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;

@property (nonatomic, strong) id<editImageViewControllerDelegate> delegate;


@property (nonatomic) PNCircleChart * BMIChart;

@property(nonatomic) UIImage *imagen;
@property(nonatomic) NSString *fechaCreacion;

@property(nonatomic) NSString *comentario;

@property (nonatomic) float longitud;
@property (nonatomic) float latitud;

@property (nonatomic, strong) PHAssetCollection *assetCollection;





@property (nonatomic) int recordIDToEdit;

@property (nonatomic) Analisis* objetoAnalisis;
@property (nonatomic) TipoAnalisis* objetoTipoAnalisis;


-(void) generarGraficaPorcentaje:(int)max minimo:(float)min;


-(void)loadInfoToEdit;
-(UIImage*) getImagenUrl:(NSString*)urlImagen;

@end
