//
//  AppDelegate.m
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 5/10/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import "AppDelegate.h"


@implementation AppDelegate

@synthesize window = _window;
@synthesize  variableEjemploDelegate;

#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad

/**
 * @brief Función que se ejecuta al arrancar la aplicación, inicializa el storyboard, y las preferencias del usuario
 * @param (UIApplication *)application
 * @param (NSDictionary *)launchOptions
 * @return (BOOL)
 */
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{    
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    Alto = _window.frame.size.height; Ancho = _window.frame.size.width;
    if ( IDIOM == IPAD ) {
        UIStoryboard *lienzo = [UIStoryboard storyboardWithName:@"Main" bundle:Nil];
        [_window setRootViewController:[lienzo instantiateInitialViewController]];
        NSLog(@"Entro en el IF - Altura: %u", Alto);
        NSLog(@"Entro en el IF - Ancho: %u", Ancho);
    }else{
        UIStoryboard *lienzo = [UIStoryboard storyboardWithName:@"iPhone" bundle:Nil];
        [_window setRootViewController:[lienzo instantiateInitialViewController]];
        NSLog(@"No entro en el IF - Altura: %u", Alto);
        NSLog(@"No entro en el IF - Ancho: %u", Ancho);
    }
    
    
    //Inicializamos preferencias del usuario
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSNumber *ultimaVisita = [defaults objectForKey:@"kUltimaVisita"];
    

    
    
    if(ultimaVisita == nil){
        

        
        
        NSDictionary *defaultValues = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [NSNumber numberWithBool:NO],@"gpsSettings",
                                       [NSNumber numberWithBool:YES],@"kUltimaVisita",
                                       [NSString stringWithFormat:@"None"],@"tipoAnalisis",
                                       [NSNumber numberWithFloat:0],@"Concentracion1",
                                       [NSNumber numberWithFloat:15],@"Concentracion2",
                                       [NSNumber numberWithFloat:30],@"Concentracion3",
                                       [NSNumber numberWithFloat:45],@"Concentracion4",
                                       [NSNumber numberWithFloat:60],@"Concentracion5",
                                       [NSNumber numberWithFloat:0],@"numAnalisis",

                                       nil];
        [defaults registerDefaults:defaultValues];
        [defaults synchronize];
    }
   
    return YES;
    
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



@end
