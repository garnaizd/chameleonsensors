//
//  confViewController.m
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 21/10/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//


#import "ConcentracionesForm.h"
#import "ConcentracionesViewController.h"

@interface ConcentracionesViewController ()



@end

@implementation ConcentracionesViewController

ConcentracionesForm *form;
NSUserDefaults *defaults;

NSMutableArray *mutableArray;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    mutableArray = [NSMutableArray array];
    
    
}


/**
 * @brief Función que se ejecuta cuando se cierra la vista, y guarda los cambios realizados por el usuario en los valores de las concentraciones
 */
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    
    form = self.formController.form;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    
    [userDefaults setObject:form.concentracion1 forKey:@"Concentracion1"];
    [userDefaults setObject:form.concentracion2 forKey:@"Concentracion2"];
    [userDefaults setObject:form.concentracion3 forKey:@"Concentracion3"];
    [userDefaults setObject:form.concentracion4 forKey:@"Concentracion4"];
    [userDefaults setObject:form.concentracion5 forKey:@"Concentracion5"];
    
    [userDefaults synchronize];
}






/**
 * @brief Función inicializa el formulario de concentraciones
 */
- (void)awakeFromNib
{
    
    //set up form
    self.formController.form = [[ConcentracionesForm alloc] init];
    
    
}




@end
