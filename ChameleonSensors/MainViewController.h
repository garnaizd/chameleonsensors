//
//  ViewController.h
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 5/10/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
#import "editImageViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "PhotoTweaksViewController.h"
#import "SettingsViewController.h"
#import "TipoAnalisisViewController.h"



@interface MainViewController :  UIViewController <UITableViewDelegate, UITableViewDataSource,UINavigationControllerDelegate, UIImagePickerControllerDelegate,editImageViewControllerDelegate,CLLocationManagerDelegate,UIPickerViewDelegate, UIPickerViewDataSource,TipoAnalisisViewControllerDelegate>



@property (weak, nonatomic) IBOutlet UITableView *tblAnalisis;


@property(nonatomic) NSString *fechaCreacion;

@property (nonatomic, strong) PHAssetCollection *assetCollection;
@property (nonatomic) int recordIDToEdit;

@property (weak, nonatomic) IBOutlet UIPickerView *ViewPicker;



-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;

@property (nonatomic, strong) NSArray *arrAnalisis;
@property (nonatomic, strong) NSArray *arrTiposAnalisis;
-(bool)esLegal:(float)ppmFe conTipoAnalisis:(NSNumber*)tipo;
@end
