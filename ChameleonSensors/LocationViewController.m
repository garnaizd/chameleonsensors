//
//  LocationViewController.m
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 30/12/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocationViewController.h"
#import "MapPin.h"
#import "UIAlertController.h"

@implementation LocationViewController

CLLocation *posicionActual;

/**
 * @brief Función que se ejecuta con la vista. Indica si ha habido algun error al inciializar el mapa.
 */
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title =NSLocalizedString(@"Mapa", nil);

    @try {
        

        if(_latitud!=0 && _longitud!=0){
            [self addAnotacion:_latitud conLongitus:_longitud];
        }else{
            UIAlertController *alertaError = [UIAlertController alertaErrorConMensaje:NSLocalizedString(@"Error al procesar las coordenadas", nil)];
        
            [self presentViewController:alertaError animated:YES completion:nil];
        }
        
    }@catch (NSException *exception) {
        UIAlertController *alertaError = [UIAlertController alertaAvisoConMensaje:NSLocalizedString(@"No se ha podido obtener la ubicación", nil)];
        [self presentViewController:alertaError animated:YES completion:nil];
        
    }
}

/**
 * @brief Función que añade un pin en la posición gps
 * @param (CLLocationDegrees)latitud de la coordenada
 * @param (CLLocationDegrees)longitud de la coordenada
 */
-(void)addAnotacion:(CLLocationDegrees)latitud conLongitus:(CLLocationDegrees)longitud
{
    
    CLLocationCoordinate2D center;
    center.latitude = latitud;
    center.longitude = longitud;
    
    MKCoordinateSpan zoom;
    zoom.latitudeDelta = .1f;
    zoom.longitudeDelta = .1f;
    
    MKCoordinateRegion myRegion;
    myRegion.center = center;
    myRegion.span = zoom;
    
    [_mapa setRegion:myRegion animated:YES];
    
    _mapa.mapType = MKMapTypeStandard;
    
    MapPin *pin = [[MapPin alloc] init];
    pin.titulo = NSLocalizedString(@"Análisis", nil);
    pin.subtitulo = NSLocalizedString(@"Coordenadas", nil);

    [_mapa addAnnotation:pin];
    pin.coordinate = center;
    
    
}



@end