//
//  AppDelegate.h
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 5/10/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DBAccess/DBAccess.h>
#import "TipoAnalisisQuery.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,DBDelegate>
{
    
    int Alto;
    int Ancho;
}

@property (strong, nonatomic) UIWindow *window;


@property (strong, nonatomic) AppDelegate *viewController;

//definición variables globales

@property (nonatomic) NSString *variableEjemploDelegate;

@end

 