//
//  ConcentracionesForm.m
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 27/12/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConcentracionesForm.h"

@interface ConcentracionesForm ()

@end


@implementation ConcentracionesForm


/**
 * @brief Función que inicializa la vista de Concentraciones
 * @param NSArray* con las celdas inicializadas
 */
- (NSArray *)fields{
    
    
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    return @[ @{FXFormFieldTitle:[NSString stringWithFormat:@"%@ %d (ppm)",NSLocalizedString(@"Concentracion",nil),1],
                FXFormFieldHeader: NSLocalizedString(@"Concentracciones", nil),
                FXFormFieldKey: @"concentracion1", FXFormFieldDefaultValue:[defaults objectForKey:@"Concentracion1"], FXFormFieldType:FXFormFieldTypeNumber},
              
              @{FXFormFieldTitle:[NSString stringWithFormat:@"%@ %d (ppm)",NSLocalizedString(@"Concentracion",nil),2],
                FXFormFieldKey: @"concentracion2",FXFormFieldDefaultValue:[defaults objectForKey:@"Concentracion2"], FXFormFieldType:FXFormFieldTypeNumber },
              
              @{FXFormFieldTitle:[NSString stringWithFormat:@"%@ %d (ppm)",NSLocalizedString(@"Concentracion",nil),3],
                FXFormFieldKey: @"concentracion3",FXFormFieldDefaultValue:[defaults objectForKey:@"Concentracion3"], FXFormFieldType:FXFormFieldTypeNumber },
              
              
              @{FXFormFieldTitle:[NSString stringWithFormat:@"%@ %d (ppm)",NSLocalizedString(@"Concentracion",nil),4],
                FXFormFieldKey: @"concentracion4",FXFormFieldDefaultValue:[defaults objectForKey:@"Concentracion4"], FXFormFieldType:FXFormFieldTypeNumber },
              
              @{FXFormFieldTitle:[NSString stringWithFormat:@"%@ %d (ppm)",NSLocalizedString(@"Concentracion",nil),5],
                FXFormFieldKey: @"concentracion5",FXFormFieldDefaultValue:[defaults objectForKey:@"Concentracion5"],
                FXFormFieldType:FXFormFieldTypeNumber,FXFormFieldFooter:NSLocalizedString(@"Estas concentraciones estan ordenadas de izquierda a derecha según están en la base del polímero, colocándo la muestra desconocida al lado izquierdo", nil) },
              
              
              ];


}

@end