//
//  openCV.m
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 30/10/15.
//  Copyright © 2015 Gema Arnaiz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "openCV.h"


@implementation openCV

using namespace cv;
using namespace std;


//tamaño elemento erosion
int erosion_size = 3;
//tamaño dilatación del elemento
int dilation_size = 2;

/**
 * @brief Convierte imagen UIImage a cv::Mat
 * @param (UIImage *)imagen
 * @return (cv::Mat) imagen
 */
+(cv::Mat) cvMatFromUIImage:(UIImage *)image
{
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    
    cv::Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels (color channels + alpha)
    
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to  data
                                                    cols,                       // Width of bitmap
                                                    rows,                       // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    
    return cvMat;
}

/**
 * @brief Convierte imagen cv::Mat a UIImage
 * @param (cv::Mat) imagen
 * @return (UIImage *)imagen
 */
+(UIImage *) UIImageFromCVMat:(cv::Mat)cvMat
{
    NSData *data = [NSData dataWithBytes:cvMat.data length:cvMat.elemSize()*cvMat.total()];
    CGColorSpaceRef colorSpace;
    
    if (cvMat.elemSize() == 1) {
        colorSpace = CGColorSpaceCreateDeviceGray();
    } else {
        colorSpace = CGColorSpaceCreateDeviceRGB();
    }
    
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    
    // Creating CGImage from cv::Mat
    CGImageRef imageRef = CGImageCreate(cvMat.cols,                                 //width
                                        cvMat.rows,                                 //height
                                        8,                                          //bits per component
                                        8 * cvMat.elemSize(),                       //bits per pixel
                                        cvMat.step[0],                            //bytesPerRow
                                        colorSpace,                                 //colorspace
                                        kCGImageAlphaNone|kCGBitmapByteOrderDefault,// bitmap info
                                        provider,                                   //CGDataProviderRef
                                        NULL,                                       //decode
                                        false,                                      //should interpolate
                                        kCGRenderingIntentDefault                   //intent
                                        );
    
    
    // Getting UIImage from CGImage
    UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace);
    
    return finalImage;
}


/**
 * @brief establece tamaño de erosion
 * @param int tamaño de la erosion
 */
+(void) setErosionSize:(int)tam
{
    erosion_size = tam;
    
}

/**
 * @brief obtiene tamaño de erosion
 * @return int tamaño erosion
 */
+(int) getErosionSize
{
    return erosion_size;
    
}

/**
 * @brief establece tamaño de dilatación
 * @param int tamaño de la dilatión
 */
+(void) setDilatationSize:(int)tam
{
    dilation_size = tam;
    
}


/**
 * @brief obtiene tamaño de dilatación
 * @return int tamaño dilatación
 */
+(int) getDilatationSize
{
    return dilation_size;
    
}



/**
 * @brief Funcion erosiona imagen Mat
 * @param (cv::Mat *)imagen
 * @param (cv::MorphShapes)forma de la figura erosión
 */
+(void) Erosion:(cv::Mat *)imagen forma:(cv::MorphShapes)forma
{
    Mat element = getStructuringElement(forma,
                                        cv::Size( 2*erosion_size + 1, 2*erosion_size+1 ),
                                        cv::Point( erosion_size, erosion_size ) );
    
    erode( *imagen, *imagen, element );
    printf("Se ha erosionado la imagen");
}



/**
 * @brief Funcion dilatación imagen Mat
 * @param (cv::Mat *)imagen
 * @param (cv::MorphShapes)forma de la figura dilatación
 */
+(void) Dilation:(cv::Mat *)imagen forma:(cv::MorphShapes)forma
{
    
    Mat element = getStructuringElement( forma,
                                        cv::Size( 2*dilation_size + 1, 2*dilation_size+1 ),
                                        cv::Point( dilation_size, dilation_size ) );
    /// Apply the dilation operation
    dilate( *imagen, *imagen, element );
    printf("Se ha dilatado la imagen");
}




/**
 * @brief Funcion convierte a gris imagen Mat
 * @param (cv::Mat *)imagen a color
 * @return (cv::Mat)imagen en gris
 */
+(cv::Mat)converToGris:(cv::Mat)imagen
{
    cv::Mat imagGris;
    cvtColor(imagen, imagGris, CV_BGR2GRAY);
    
    return imagGris;
}

/**
 * @brief Funcion cambia formato imagen
 * @param (int)formato
 * @param (cv::Mat)imagen
 * @return (cv::Mat)imagen cambiada formato
 */
+(cv::Mat)converTo:(int)tipo conImagen:(cv::Mat)imagen
{
    cv::Mat imgSalida;
    imagen.convertTo(imgSalida, tipo);
    
    return imgSalida;
}

/**
 * @brief Funcion filtro gaussiano
 * @param (cv::Mat)imagen
 * @param (cv::Size)tamFiltro
 * @param (double)sigmaX
 * @param (double)sigmaY
 * @return (cv::Mat)imagen suavizada con el filtro
 */
+(cv::Mat)filtroGaussiano:(cv::Mat)imagen conTamFiltro:(cv::Size)tamFiltro conSigmaX:(double)sigmaX conSigmaY:(double)sigmaY
{
    cv::Mat imgSalida;
    cv::GaussianBlur(imagen, imgSalida,tamFiltro, sigmaX,sigmaY);
    
    return imgSalida;
}

/**
 * @brief Funcion filtro gaussiano
 * @param (cv::Mat)imagen
 * @param (cv::Size)tamFiltro
 * @param (double)sigmaX
 * @param (double)sigmaY
 * @return (cv::Mat)imagen suavizada con el filtro
 */
+(cv::Mat)algoritmoCanny:(cv::Mat)imagen conUmbralCanny:(double)umbralCanny conRatioCanny:(double)ratioCanny conGradiente:(BOOL)gradiente
{
    cv::Mat imgSalida;
    cv::Canny(imagen, imgSalida, umbralCanny, umbralCanny*ratioCanny, ratioCanny ,gradiente);
    return imgSalida;
}



/**
 * @brief Funcion que calcula la media de colores rgb de una matriz Mat
 * @param (cv::Mat)imagen
 * @param (int)radio
 * @param (cv::Point2f)centro
 * @return (cv::Scalar) media de pixeles RGB
 */
+(cv::Scalar) calcularMediaColores:(cv::Mat)imagen radio:(int)radio centro:(cv::Point2f)centro
{
    //origen para recorrer el cuadrado
    cv::Point2f origen;
    origen.x = centro.x - round(radio/2);
    origen.y = centro.y - round(radio/2);
    
    Mat porcionImagen = imagen(cv::Rect(origen.x,origen.y,radio,radio));
    cv::Scalar ab=cv::mean(porcionImagen);
    return ab;
}


@end
