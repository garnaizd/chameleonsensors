//
//  ViewControllerSpec.m
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 15/1/16.
//  Copyright © 2016 Gema Arnaiz. All rights reserved.
//
//

#import <Expecta/Expecta.h>
#import <Specta/Specta.h>
#import <OCMock/OCMock.h>

#import "MainViewController.h"

@interface MainViewController(Private)
@property(nonatomic,strong)NSArray *arrAnalisis;
@end


SpecBegin(MainViewControllerSpec)


describe(@"MainViewControllerTest", ^{
    __block MainViewController *mvc;

    beforeAll(^{
        UIStoryboard * mainSrotyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController * controlador = [mainSrotyboard instantiateViewControllerWithIdentifier:@"Root"];
        
        mvc = (MainViewController*)controlador;
        [mvc view];

    });
    
    it(@"Debería existir el controlador ViewController (Root)",^{
    
        expect(mvc).toNot.beNil();

    });
    
    it(@"Debería conformar con UITableViewDataSource y el protocolo de delegación",^{
        
        expect(mvc).conformTo(@protocol(UITableViewDataSource));
        expect(mvc).conformTo(@protocol(UITableViewDelegate));
    });
    
    it(@"Debería responder a los métodos requeridos por UITableViewDataSource",^{
        
        expect(mvc).respondTo(@selector(tableView:numberOfRowsInSection:));
        expect(mvc).respondTo(@selector(tableView:cellForRowAtIndexPath:));
    });
    
    it(@"Debería haber una TableView en MainViewController",^{
        
        expect(mvc.tblAnalisis).notTo.beNil();
    });
    
    it(@"Debería de detectar el número de celdas",^{
        NSArray * datos=@[@"Análisis1",@"Análisis2",@"Análisis3",@"Análisis4",@"Análisis5",@"Análisis6"];
        
        mvc.arrAnalisis = datos;
        
        [mvc.tblAnalisis reloadData];
        
        NSInteger numFilas =[mvc tableView:mvc.tblAnalisis numberOfRowsInSection:0];
 
        expect(numFilas).to.equal(6);
    });
    
    //UimagePicker
    it(@"Debería conformar con el protocolo de delegación",^{
        
        expect(mvc).conformTo(@protocol(UIImagePickerControllerDelegate));
    });
    


    
});

SpecEnd
