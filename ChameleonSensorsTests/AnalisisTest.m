//
//  AnalisisTest.m
//  ChameleonSensors
//
//  Created by Gema Arnaiz on 15/1/16.
//  Copyright © 2016 Gema Arnaiz. All rights reserved.
//

#import <Expecta/Expecta.h>
#import <Specta/Specta.h>
#import <OCMock/OCMock.h>

#import "AnalisisQuery.h"



SpecBegin(AnalisisSpec)


describe(@"AnalisisTest", ^{
    __block Analisis* analisis;
    __block id analisisQuery;
    beforeAll(^{
        
         analisis = [Analisis new];
        
                
        analisisQuery = [OCMockObject mockForClass:[AnalisisQuery class]];

        analisisQuery = [[AnalisisQuery alloc]
                            initWithDatabaseFilename:@"MockBaseDatos"];
    });
    
    it(@"Debería existir el objeto análisis",^{
        
        expect(analisis).toNot.beNil();
        
    });
    
    it(@"Debería Haberse inicializado la base de datos MockBaseDatos",^{
        
        expect(analisisQuery).toNot.beNil();
        
    });
    


    
    
    


    
});

SpecEnd